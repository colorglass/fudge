#pragma once

#include <FDGE/Config/PluginConfig.h>
#include <FDGE/Config/Proxy.h>

namespace FDGE {
    gluino_enum(CosaveFormat, uint32_t,
        (Auto, 0),
        (YAML, 1),
        (YAMLWithZlib, 2),
        (YAMLWithZstd, 3));

    class Cosave {
    public:
        [[nodiscard]] inline CosaveFormat GetFormat() const noexcept {
            return _format;
        }

        inline void SetFormat(CosaveFormat format) noexcept {
            _format = format;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_format, "format");
        }

        CosaveFormat _format{CosaveFormat::Auto};

        friend class articuno::access;
    };

    class Modules {
    public:
        [[nodiscard]] inline bool IsDymodsEnabled() const noexcept {
            return _enableDymods;
        }

        inline void SetDymodsEnabled(bool dymodsEnabled) noexcept {
            _enableDymods = dymodsEnabled;
        }

        [[nodiscard]] inline bool IsScriptObjectsEnabled() const noexcept {
            return _enableScriptObjects;
        }

        inline void SetScriptObjectsEnabled(bool scriptObjectsEnabled) noexcept {
            _enableScriptObjects = scriptObjectsEnabled;
        }

        [[nodiscard]] inline bool IsConsoleCommandsEnabled() const noexcept {
            return _enableConsoleCommands;
        }

        inline void SetConsoleCommandsEnabled(bool consoleCommandsEnabled) noexcept {
            _enableConsoleCommands = consoleCommandsEnabled;
        }

        [[nodiscard]] inline bool IsSerializationEnabled() const noexcept {
            return _enableSerialization;
        }

        inline void SetSerializationEnabled(bool serializationEnabled) noexcept {
            _enableSerialization = serializationEnabled;
        }

        [[nodiscard]] inline bool IsEditorIDPreservationEnabled() const noexcept {
            return _enableEditorIDPreservation;
        }

        inline void SetEditorIDPreservationEnabled(bool editorIDPreservationEnabled) noexcept {
            _enableEditorIDPreservation = editorIDPreservationEnabled;
        }

        [[nodiscard]] inline bool IsReverseLookupDatabaseEnabled() const noexcept {
            return _enableReverseLookupDatabase;
        }

        inline void SetReverseLookupDatabaseEnabled(bool reverseLookupDatabaseEnabled) noexcept {
            _enableReverseLookupDatabase = reverseLookupDatabaseEnabled;
        }

        [[nodiscard]] inline bool IsDisplayEnabled() const noexcept {
            return _enableDisplay;
        }

        inline void SetDisplayEnabled(bool displayEnabled) noexcept {
            _enableDisplay = displayEnabled;
        }

        [[nodiscard]] inline bool IsExteriorCellAnalysisEnabled() const noexcept {
            return _enableExteriorCellAnalysis;
        }

        inline void SetExteriorCellAnalysisEnabled(bool exteriorCellAnalysisEnabled) noexcept {
            _enableExteriorCellAnalysis = exteriorCellAnalysisEnabled;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_enableDymods, "enableDymods");
            ar <=> articuno::kv(_enableScriptObjects, "enableScriptObjects");
            ar <=> articuno::kv(_enableConsoleCommands, "enableConsoleCommands");
            ar <=> articuno::kv(_enableSerialization, "enableSerialization");
            ar <=> articuno::kv(_enableReverseLookupDatabase, "enableEditorIDPreservation");
            ar <=> articuno::kv(_enableReverseLookupDatabase, "enableReverseLookupDatabase");
            ar <=> articuno::kv(_enableDisplay, "enableDisplay");
            ar <=> articuno::kv(_enableExteriorCellAnalysis, "enableExteriorCellAnalysis");
        }

        bool _enableDymods{true};
        bool _enableScriptObjects{true};
        bool _enableConsoleCommands{true};
        bool _enableSerialization{true};
        bool _enableEditorIDPreservation{true};
        bool _enableReverseLookupDatabase{true};
        bool _enableDisplay{true};
        bool _enableExteriorCellAnalysis{true};

        friend class articuno::access;
    };

    class PapyrusTesting {
    public:
        [[nodiscard]] inline bool IsEnabled() const noexcept {
            return _enabled;
        }

        inline void SetEnabled(bool enabled) noexcept {
            _enabled = enabled;
        }

        [[nodiscard]] inline bool GetRunOnNewGame() const noexcept {
            return _runOnNewGame;
        }

        inline void SetRunOnNewGame(bool runOnNewGame) noexcept {
            _runOnNewGame = runOnNewGame;
        }

        [[nodiscard]] inline bool GetRunOnLoad() const noexcept {
            return _runOnLoad;
        }

        inline void SetRunOnLoad(bool runOnLoad) noexcept {
            _runOnLoad = runOnLoad;
        }

        [[nodiscard]] inline bool IsLoggingEnabled() const noexcept {
            return _logging;
        }

        inline void SetLoggingEnabled(bool loggingEnabled) noexcept {
            _logging = loggingEnabled;
        }

        [[nodiscard]] inline bool IsJUnitEnabled() const noexcept {
            return _junit;
        }

        inline void SetJUnitEnabled(bool junitEnabled) noexcept {
            _junit = junitEnabled;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_enabled, "enabled");
            ar <=> articuno::kv(_runOnNewGame, "runOnNewGame");
            ar <=> articuno::kv(_runOnLoad, "runOnLoad");
            ar <=> articuno::kv(_logging, "logging");
            ar <=> articuno::kv(_junit, "junit");
        }

        bool _enabled{true};
        bool _runOnNewGame{false};
        bool _runOnLoad{false};
        bool _logging{true};
        bool _junit{true};

        friend class articuno::access;
    };

    class DebugConfig : public Config::DebugConfig {
    public:
        [[nodiscard]] inline Modules& GetModules() noexcept {
            return _modules;
        }

        [[nodiscard]] inline const Modules& GetModules() const noexcept {
            return _modules;
        }

        [[nodiscard]] inline PapyrusTesting& GetPapyrusTesting() noexcept {
            return _papyrusTesting;
        }

        [[nodiscard]] inline const PapyrusTesting& GetPapyrusTesting() const noexcept {
            return _papyrusTesting;
        }

    private:
        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_modules, "modules");
            ar <=> articuno::kv(_papyrusTesting, "papyrusTesting");
            articuno_super(Config::DebugConfig, ar, flags);
        }

        Modules _modules;
        PapyrusTesting _papyrusTesting;

        friend class articuno::access;
    };

    class PerformanceConfig {
    public:
        [[nodiscard]] inline std::size_t GetEditorIDReservation() const noexcept {
            return _editorIDReservation;
        }

        inline void SetEditorIDReservation(std::size_t editorIDReservation) noexcept {
            _editorIDReservation = editorIDReservation;
        }

        [[nodiscard]] inline std::size_t GetCellReservation() const noexcept {
            return _cellReservation;
        }

        inline void SetCellReservation(std::size_t cellReservation) noexcept {
            _cellReservation = cellReservation;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_editorIDReservation, "editorIdReservation");
            ar <=> articuno::kv(_cellReservation, "cellReservation");
        }

        std::size_t _editorIDReservation{1048576};
        std::size_t _cellReservation{1048576};

        friend class articuno::access;
    };

    class UIConfig {
    public:
        [[nodiscard]] inline std::optional<uint32_t> GetMaximumWidth() const noexcept {
            return _maximumWidth;
        }

        inline void SetMaximumWidth(std::optional<uint32_t> maximumWidth) noexcept {
            _maximumWidth = maximumWidth;
        }

        [[nodiscard]] inline std::optional<uint32_t> GetMaximumHeight() const noexcept {
            return _maximumHeight;
        }

        inline void SetMaximumHeight(std::optional<uint32_t> maximumHeight) noexcept {
            _maximumHeight = maximumHeight;
        }

        [[nodiscard]] inline bool IsDebug() const noexcept {
            return _debug;
        }

        inline void SetDebug(bool debug) noexcept {
            _debug = debug;
        }

        [[nodiscard]] inline uint8_t GetSampleCount() const noexcept {
            return _sampleCount;
        }

        inline void SetSampleCount(uint8_t sampleCount) noexcept {
            _sampleCount = sampleCount;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_maximumWidth, "maximumWidth");
            ar <=> articuno::kv(_maximumHeight, "maximumHeight");
            ar <=> articuno::kv(_debug, "debug");
        }

        std::optional<uint32_t> _maximumWidth;
        std::optional<uint32_t> _maximumHeight;
        bool _debug{false};
        uint8_t _sampleCount{1};

        friend class articuno::access;
    };

    class FDGEConfig : public Config::PluginConfig<DebugConfig> {
    public:
        [[nodiscard]] inline PerformanceConfig& GetPerformance() noexcept {
            return _performance;
        }

        [[nodiscard]] inline const PerformanceConfig& GetPerformance() const noexcept {
            return _performance;
        }

        [[nodiscard]] inline Cosave& GetCosave() noexcept {
            return _cosave;
        }

        [[nodiscard]] inline const Cosave& GetCosave() const noexcept {
            return _cosave;
        }

        [[nodiscard]] inline UIConfig& GetUI() noexcept {
            return _ui;
        }

        [[nodiscard]] inline const UIConfig& GetUI() const noexcept {
            return _ui;
        }

        static Config::Proxy<FDGEConfig>& GetProxy() noexcept;

    private:
        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_performance, "performance");
            ar <=> articuno::kv(_cosave, "cosave");
            ar <=> articuno::kv(_ui, "ui");
            articuno_super(Config::PluginConfig<DebugConfig>, ar, flags);
        }

        PerformanceConfig _performance;
        Cosave _cosave;
        UIConfig _ui;

        friend class articuno::access;
    };
}

namespace articuno::serde {
    template <::articuno::serializing_archive Archive>
    void serde(Archive& ar, const FDGE::CosaveFormat& value, articuno::value_flags) {
        std::string_view str;
        switch (static_cast<FDGE::CosaveFormat::enum_type>(value)) {
            case FDGE::CosaveFormat::enum_type::Auto:
                str = "auto";
            case FDGE::CosaveFormat::enum_type::YAML:
                str = "yaml";
            case FDGE::CosaveFormat::enum_type::YAMLWithZstd:
                str = "yaml+zstd";
        }
        ar <=> articuno::self(str);
    }

    template <::articuno::deserializing_archive Archive>
    void serde(Archive& ar, FDGE::CosaveFormat& value, articuno::value_flags) {
        std::string str;
        gluino::str_equal_to<false> equals;
        if ((ar <=> ::articuno::self(str))) {
            if (equals(str, "auto")) {
                value = FDGE::CosaveFormat::Auto;
            } else if (equals(str, "yaml")) {
                value = FDGE::CosaveFormat::YAML;
            } else if (equals(str, "yaml+zlib")) {
                value = FDGE::CosaveFormat::YAMLWithZlib;
            } else if (equals(str, "yaml+zstd")) {
                value = FDGE::CosaveFormat::YAMLWithZstd;
            }
        } else {
            value = FDGE::CosaveFormat::Auto;
        }
    }
}
