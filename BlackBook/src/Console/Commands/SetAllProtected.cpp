#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace RE;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"SetAllProtected", "saprot"}, [](Execution& execution) {
                        execution.NoExplicitTarget();
                        auto value = execution.NextPositional<bool>("value");

                        for (auto actorHandle : RE::ProcessLists::GetSingleton()->highActorHandles) {
                            auto actor = actorHandle.get();
                            if (!actor) {
                                continue;
                            }
                            if (value) {
                                actor->boolFlags.set(Actor::BOOL_FLAGS::kProtected);
                            } else {
                                actor->boolFlags.reset(Actor::BOOL_FLAGS::kProtected);
                            }
                            auto* base = actor->GetActorBase()->As<TESActorBaseData>();
                            if (value) {
                                base->actorData.actorBaseFlags.set(ACTOR_BASE_DATA::Flag::kProtected);
                            } else {
                                base->actorData.actorBaseFlags.reset(ACTOR_BASE_DATA::Flag::kProtected);
                            }
                        }
                    }));
        });
    }
}
