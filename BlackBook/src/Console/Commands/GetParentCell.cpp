#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Skyrim;
using namespace RE;

namespace {
    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(
                    CommandSpec("fdge", {"GetParentCell", "getcell"}, [](Execution& execution) {
                        auto* target = execution.OptionalTarget<TESObjectREFR*>(PlayerCharacter::GetSingleton());
                        auto* cell = target->GetParentCell();

                        if (!cell) {
                            Print("Unable to determine parent cell.");
                            return;
                        }

                        auto* edid = cell->GetFormEditorID();
                        if (edid && edid[0] != '\0') {
                            Print("Current Cell: {:X} ({})",  cell->GetFormID(), edid);
                        } else {
                            Print("Current Cell: {:X}", cell->GetFormID());
                        }
                    }));
        });
    }
}
