#include <FDGE/Console/CommandRepository.h>
#include <FDGE/Console/Console.h>
#include <FDGE/Plugin.h>
#include <FDGE/Skyrim/FormType.h>
#include <FDGE/System.h>
#include <FDGE/Util/Maps.h>

using namespace FDGE;
using namespace FDGE::Console;
using namespace FDGE::Util;
using namespace gluino;
using namespace phmap;
using namespace RE;
using namespace srell;

namespace {
    inline void HandleForm(bool useRegex, bool useExact, bool usePrefix, bool useSuffix, bool useEditorID,
                           bool useFullName, const regex& pattern, BSFixedString text, TESForm* form,
                           istr_btree_map<std::string_view, btree_map<FormID, std::string>>& results) {
        if (!form) {
            return;
        }

        BSFixedString matchedEditorID;
        if (!useFullName) {
            auto edid = case_insensitive_string_view(form->GetFormEditorID());
            if (useRegex) {
                if (regex_search(edid.data(), pattern)) {
                    matchedEditorID = edid.data();
                }
            } else {
                if ((useExact && edid == text) || (usePrefix && edid.starts_with(text)) ||
                    (useSuffix && edid.ends_with(text)) ||
                    (!usePrefix && !useSuffix && !useExact && edid.contains(text))) {
                    matchedEditorID = edid.data();
                }
            }
        }
        BSFixedString matchedName;
        if (!useEditorID) {
            auto* fullNameForm = skyrim_cast<TESFullName*>(form);
            if (!fullNameForm) {
                return;
            }
            auto fullName = case_insensitive_string_view(fullNameForm->GetFullName());
            if (useRegex) {
                if (regex_search(fullName.data(), pattern)) {
                    matchedName = fullName.data();
                }
            } else {
                if ((useExact && fullName == text) || (usePrefix && fullName.starts_with(text)) ||
                    (useSuffix && fullName.ends_with(text)) ||
                    (!usePrefix && !useSuffix && !useExact && fullName.contains(text))) {
                    matchedName = fullName.data();
                }
            }
        }
        auto recordName = FDGE::Skyrim::FormType(form->GetFormType()).GetRecordName();
        auto entryList = results.try_emplace(recordName, btree_map<FormID, std::string>{});
        const auto* fileName =
            form->sourceFiles.array && !form->sourceFiles.array->empty() ? form->GetFile(0)->fileName : "None";
        if (matchedEditorID.empty()) {
            if (matchedName.empty()) {
                return;
            }
            entryList.first->second.try_emplace(form->GetFormID(),
                    fmt::format(R"({}: {:X} (Name: "{}", File: "{}"))", recordName, form->GetFormID(),
                            matchedName.c_str(), fileName));
        } else if (matchedName.empty()) {
            entryList.first->second.try_emplace(form->GetFormID(),
                                                fmt::format(R"({}: {:X} (EditorID: "{}", File: "{}"))", recordName,
                                                            form->GetFormID(), matchedEditorID.c_str(), fileName));
        } else {
            entryList.first->second.try_emplace(
                form->GetFormID(),
                fmt::format(R"({}: {:X} (Name: "{}", EditorID: "{}", File: "{}"))", recordName, form->GetFormID(),
                            matchedName.c_str(), matchedEditorID.c_str(), fileName));
        }
    }

    OnSKSELoaded {
        System().AfterDataLoaded([]() {
            CommandRepository::GetSingleton().RegisterForever(CommandSpec("fdge", {"search"}, [](Execution& execution) {
                execution.NoExplicitTarget();

                auto useRegex = execution.Flag("regex", "r");
                auto usePrefix = execution.Flag("prefix", "p");
                auto useSuffix = execution.Flag("suffix", "s");
                auto useExact = execution.Flag("exact", "x");

                if (static_cast<int>(useRegex) + usePrefix + useSuffix + useExact > 1) {
                    throw std::invalid_argument(
                        "Only one of 'regex', 'prefix', 'suffix', or 'exact' flags can be provided.");
                }

                auto formType = execution.OptionalParameter<Skyrim::FormType>("type", FormType::None);
                bool useEditorID = execution.Flag("edid", "e");
                bool useFullName = execution.Flag("name", "n");
                if (useEditorID && useFullName) {
                    throw std::invalid_argument(
                        "Flags '--edid' (or '-e') and '--name' (or '-n') cannot be used together.");
                }
                auto text = BSFixedString(execution.NextPositional<std::string>("text"));
                regex pattern(text.c_str(), regex::icase | regex::optimize);
                if (text.empty()) {
                    Print("Text to search for must not be empty.");
                    return;
                }

                istr_btree_map<std::string_view, btree_map<FormID, std::string>> results;

                if (useRegex) {
                    Print("Beginning search, this may take a while...");
                } else {
                    Print("Beginning search...");
                }
                if (formType == FormType::None) {
                    auto allForms = TESForm::GetAllForms();
                    BSReadLockGuard lock(allForms.second.get());
                    for (auto& entry : *allForms.first) {
                        HandleForm(useRegex, useExact, usePrefix, useSuffix, useEditorID, useFullName, pattern, text,
                                   entry.second, results);
                    }
                } else {
                    auto& forms = TESDataHandler::GetSingleton()->GetFormArray(formType);
                    for (auto& entry : forms) {
                        HandleForm(useRegex, useExact, usePrefix, useSuffix, useEditorID, useFullName, pattern, text,
                                   entry, results);
                    }
                }

                for (auto& result : results) {
                    for (auto& line : result.second) {
                        Print(line.second);
                    }
                }
            }));
        });
    }
}  // namespace
