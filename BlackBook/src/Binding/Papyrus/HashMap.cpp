#include <FDGE/Binding/Papyrus/HashMap.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

Any* HashMap::Get(Any* key) noexcept {
    std::unique_lock lock(_lock);
    auto result = _value.find(key);
    if (result == _value.end()) {
        return nullptr;
    }
    return result.value().GetObject();
}

Any* HashMap::Put(Any* key, Any* value) {
    std::unique_lock lock(_lock);
    ++_version;
    auto result = _value.try_emplace(key, value);
    if (result.second) {
        return nullptr;
    }
    auto replaced = result.first.value();
    result.first.value() = value;
    return replaced.GetObject();
}

bool HashMap::TryPut(Any* key, Any* value) {
    std::unique_lock lock(_lock);
    auto result = _value.try_emplace(key, value);
    if (result.second) {
        ++_version;
    }
    return result.second;
}

bool HashMap::Contains(Any* key) noexcept {
    std::unique_lock lock(_lock);
    return _value.find(key) != _value.end();
}

Any* HashMap::Delete(Any* key) noexcept {
    std::unique_lock lock(_lock);
    auto result = _value.find(key);
    if (result == _value.end()) {
        return nullptr;
    }
    ++_version;
    auto* deleted = result.value().GetObject();
    _value.erase(result);
    return deleted;
}

bool HashMap::Clear() noexcept {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return false;
    }
    ++_version;
    _value.clear();
    return true;
}

void HashMap::Reserve(int32_t capacity) {
    std::unique_lock lock(_lock);
    _value.rehash(capacity);
    ++_version;
}

int32_t HashMap::GetSize() const noexcept {
    return static_cast<int32_t>(_value.size());
}

ValueIterator* HashMap::GetValueIterator() noexcept {
    return GetKeyValueIterator();
}

KeyValueIterator* HashMap::GetKeyValueIterator() noexcept {
    return new HashMapIterator(this);
}

HashMapIterator::HashMapIterator(HashMap* parent, bool reverse) : _parent(parent), _reverse(reverse) {
    std::unique_lock lock(parent->_lock);
    _version = parent->_version;
    if (parent->GetSize()) {
        _iter = reverse ? parent->_value.end() - 1 : parent->_value.begin();
        if (IsValid()) {
            _key = _iter.key();
            _value = _iter.value();
        }
    } else {
        _iter = parent->_value.end();
        if (_reverse) {
            _beforeBegin = true;
        }
    }
}

HashMapIterator::HashMapIterator(HashMap* parent, bool reverse, Any* key) : _parent(parent), _reverse(reverse) {
    std::unique_lock lock(parent->_lock);
    _version = parent->_version;
    _iter = parent->_value.find(key);
    if (IsValid()) {
        _key = _iter.key();
        _value = _iter.value();
    } else if (reverse) {
        _beforeBegin = true;
    }
}

void HashMapIterator::OnGameLoadComplete() {
    auto* parent = _parent.GetObject();
    auto* key = _key.GetObject();
    if (!parent || !_key) {
        return;
    }
    std::unique_lock lock(parent->_lock);
    _iter = parent->_value.find(key);
}

bool HashMapIterator::IsValid() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    return !_beforeBegin && _version == parent->_version && _iter >= parent->_value.begin() &&
           _iter < parent->_value.end();
}

bool HashMapIterator::HasMore() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    return !_beforeBegin && _iter != parent->_value.end();
}

bool HashMapIterator::SetValue(Any* value) noexcept {
    HashMap* parent = _parent;
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    if (!IsValid()) {
        return false;
    }
    _version = ++parent->_version;
    parent->_value[_key] = value;
    return true;
}

bool HashMapIterator::Next() noexcept {
    auto* parent = _parent.GetObject();
    if (!parent) {
        return false;
    }
    std::unique_lock lock(parent->_lock);
    if (parent->_version != _version) {
        return false;
    }
    if (_reverse) {
        if (_iter == parent->_value.begin()) {
            _beforeBegin = true;
            _key = {};
            _value = {};
            return false;
        }
        --_iter;
    } else if (_iter != parent->_value.end()) {
        ++_iter;
    }
    if (!IsValid()) {
        _key = {};
        _value = {};
        return false;
    }
    _key = _iter.key();
    _value = _iter.value();
    return true;
}

Pair* HashMapIterator::GetEntry() noexcept {
    if (!_key && !_value) {
        return nullptr;
    }
    return new Pair(_key, _value);
}

namespace {
    RegisterScriptType(HashMap)
    RegisterScriptType(HashMapIterator)

    PapyrusClass(HashMap) {
        PapyrusStaticFunction(Create, int32_t initialCapacity) -> HashMap* {
            if (initialCapacity < 0) {
                return nullptr;
            }
            auto* map = new HashMap();
            map->Reserve(initialCapacity);
            return map;
        };

        PapyrusFunction(Reserve, HashMap* self, int32_t capacity) {
            if (self && capacity > 0) {
                self->Reserve(capacity);
            }
        };

        PapyrusFunction(GetReverseKeyValueIterator, HashMap* self) -> KeyValueIterator* {
            if (!self) {
                return nullptr;
            }
            return new HashMapIterator(self, true);
        };

        PapyrusFunction(GetKeyValueIteratorFrom, HashMap* self, Any* key) -> KeyValueIterator* {
            if (!self) {
                return nullptr;
            }
            return new HashMapIterator(self, false, key);
        };

        PapyrusFunction(GetReverseKeyValueIteratorFrom, HashMap* self, Any* key) -> KeyValueIterator* {
            if (!self) {
                return nullptr;
            }
            return new HashMapIterator(self, true, key);
        };
    }
}
