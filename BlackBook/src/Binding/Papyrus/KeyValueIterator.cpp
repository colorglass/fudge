#include <FDGE/Binding/Papyrus/KeyValueIterator.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

Any* KeyValueIterator::GetValue() noexcept {
    auto* entry = GetEntry();
    return entry ? entry->GetSecond() : nullptr;
}

Any* KeyValueIterator::GetKey() noexcept {
    auto* entry = GetEntry();
    return entry ? entry->GetFirst() : nullptr;
}

Any* KeyValueIterator::GetValueAndNext() noexcept {
    auto* entry = GetEntryAndNext();
    return entry ? entry->GetSecond() : nullptr;
}

Any* KeyValueIterator::NextAndGetValue() noexcept {
    auto* entry = NextAndGetEntry();
    return entry ? entry->GetSecond() : nullptr;
}

Any* KeyValueIterator::GetKeyAndNext() noexcept {
    auto* entry = GetEntryAndNext();
    return entry ? entry->GetFirst() : nullptr;
}

Any* KeyValueIterator::NextAndGetKey() noexcept {
    auto* entry = NextAndGetEntry();
    return entry ? entry->GetFirst() : nullptr;
}

Pair* KeyValueIterator::GetEntryAndNext() noexcept {
    auto* result = GetEntry();
    Next();
    return result;
}

Pair* KeyValueIterator::NextAndGetEntry() noexcept {
    Next();
    return GetEntry();
}

namespace {
    RegisterScriptType(KeyValueIterator);

    PapyrusClass(KeyValueIterator) {
        PapyrusFunction(__GetMapKey, KeyValueIterator* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->GetKey();
        };

        PapyrusFunction(__GetEntry, KeyValueIterator* self) -> Pair* {
            if (!self) {
                return nullptr;
            }
            return self->GetEntry();
        };

        PapyrusFunction(GetMapKeyAndNext, KeyValueIterator* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->GetKeyAndNext();
        };

        PapyrusFunction(NextAndGetMapKey, KeyValueIterator* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->NextAndGetKey();
        };

        PapyrusFunction(GetEntryAndNext, KeyValueIterator* self) -> Pair* {
            if (!self) {
                return nullptr;
            }
            return self->GetEntryAndNext();
        };

        PapyrusFunction(NextAndGetEntry, KeyValueIterator* self) -> Pair* {
            if (!self) {
                return nullptr;
            }
            return self->NextAndGetEntry();
        };
    };
}
