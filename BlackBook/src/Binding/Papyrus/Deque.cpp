#include <FDGE/Binding/Papyrus/Deque.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

ScriptObjectHandle<Any> Deque::GetValue(std::int32_t index) noexcept {
    std::unique_lock lock(_lock);
    if (index < 0) {
        index = GetSize() + index;
    }
    if (index < 0 || index >= GetSize()) {
        return {};
    }
    return _value[index];
}

bool Deque::SetValue(std::int32_t index, Any* value) noexcept {
    std::unique_lock lock(_lock);
    if (index < 0) {
        index = GetSize() + index;
    }
    if (index < 0 || index >= GetSize()) {
        return false;
    }
    _value[index] = value;
    ++_version;
    return true;
}

void Deque::Append(Any* value) noexcept {
    std::unique_lock lock(_lock);
    _value.emplace_back(value);
    ++_version;
}

void Deque::Prepend(Any* value) noexcept {
    std::unique_lock lock(_lock);
    _value.emplace_front(value);
    ++_version;
}

Any* Deque::PopFront() noexcept {
    std::unique_lock lock(_lock);
    if (GetSize()) {
        auto result = _value.front();
        _value.pop_front();
        ++_version;
        return result;
    }
    return nullptr;
}

Any* Deque::PopBack() noexcept {
    std::unique_lock lock(_lock);
    if (GetSize()) {
        auto result = _value.back();
        _value.pop_back();
        ++_version;
        return result;
    }
    return nullptr;
}

Any* Deque::Erase(int32_t index) noexcept {
    std::unique_lock lock(_lock);
    if (index < 0) {
        index = GetSize() + index;
    }
    if (index < 0 || index >= GetSize()) {
        return nullptr;
    }
    auto result = _value[index];
    _value.erase(_value.begin() + index);
    ++_version;
    return result;
}

int32_t Deque::GetSize() const noexcept {
    return static_cast<int32_t>(_value.size());
}

bool Deque::Clear() noexcept {
    std::unique_lock lock(_lock);
    if (!_value.empty()) {
        _value.clear();
        ++_version;
        return true;
    }
    return false;
}

bool Deque::Contains(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    for (auto& entry : _value) {
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                return true;
            }
        } else if (value->Equals(candidate)) {
            return true;
        }
    }
    return false;
}

int32_t Deque::Count(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    int32_t result = 0;
    for (auto& entry : _value) {
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                ++result;
            }
        } else if (value->Equals(candidate)) {
            ++result;
        }
    }
    return result;
}

int32_t Deque::IndexOf(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    int32_t index = 0;
    for (auto& entry : _value) {
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                return index;
            }
        } else if (value->Equals(candidate)) {
            return index;
        }
        ++index;
    }
    return -1;
}

int32_t Deque::ReverseIndexOf(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    for (int32_t index = static_cast<int32_t>(_value.size()) - 1; index >= 0; --index) {
        auto& entry = _value[index];
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                return index;
            }
        } else if (value->Equals(candidate)) {
            return index;
        }
    }
    return -1;
}

ValueIterator* Deque::GetValueIterator() noexcept {
    return new DequeIterator(this, false);
}

ValueIterator* Deque::GetReverseValueIterator() noexcept {
    return new DequeIterator(this, true);
}

ValueIterator* Deque::GetValueIteratorFrom(int32_t index) noexcept {
    return new DequeIterator(this, false, index);
}

ValueIterator* Deque::GetReverseValueIteratorFrom(int32_t index) noexcept {
    return new DequeIterator(this, true, index);
}

DequeIterator::DequeIterator(ScriptObjectHandle<Deque> parent, bool reverse) noexcept
        : _parent(std::move(parent)), _reverse(reverse) {
    Deque* deque = _parent;
    std::unique_lock lock(deque->_lock);
    _version = deque->_version;
    if (_reverse) {
        _index = deque->GetSize() - 1;
    }
    if (IsValid()) {
        _value = deque->GetValue(static_cast<int32_t>(_index));
    }
}

DequeIterator::DequeIterator(ScriptObjectHandle<Deque> parent, bool reverse,
                                           int32_t index) noexcept : _parent(std::move(parent)), _reverse(reverse) {
    Deque* deque = _parent;
    std::unique_lock lock(deque->_lock);
    if (index < 0) {
        index = deque->GetSize() + index;
    }
    if (_reverse) {
        index = deque->GetSize() - index - 1;
    }
    _index = index;
    _version = deque->_version;
    if (IsValid()) {
        _value = deque->GetValue(static_cast<int32_t>(_index));
    }
}

bool DequeIterator::IsValid() noexcept {
    Deque* deque = _parent;
    return deque->GetVersion() == _version && _index < deque->GetSize() && _index >= 0;
}

bool DequeIterator::HasMore() noexcept {
    Deque* deque = _parent;
    return _index < deque->GetSize() && _index >= 0;
}

Any* DequeIterator::GetValue() noexcept {
    return _value.GetObject();
}

bool DequeIterator::SetValue(Any* value) noexcept {
    Deque* deque = _parent;
    std::unique_lock lock(deque->_lock);
    if (!IsValid()) {
        return false;
    }
    _version = ++deque->_version;
    deque->_value[_index] = value;
    return true;
}

bool DequeIterator::Next() noexcept {
    Deque* deque = _parent;
    std::unique_lock lock(deque->_lock);
    _reverse ? --_index : ++_index;
    if (!IsValid()) {
        _value = {};
        return false;
    }
    _value = deque->GetValue(static_cast<int32_t>(_index));
    return true;
}

namespace {
    RegisterScriptType(Deque);
    RegisterScriptType(DequeIterator);

    PapyrusClass(Deque) {
        PapyrusStaticFunction(Create) {
            return new Deque();
        };

        PapyrusFunction(Append, Deque* self, Any* value) {
            if (self) {
                self->Append(value);
            }
        };

        PapyrusFunction(Prepend, Deque* self, Any* value) {
            if (self) {
                self->Prepend(value);
            }
        };

        PapyrusFunction(PopFront, Deque* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->PopFront();
        };

        PapyrusFunction(PopBack, Deque* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->PopBack();
        };
    };
}
