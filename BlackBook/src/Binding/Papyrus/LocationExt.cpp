#include <FDGE/Binding/Papyrus/PapyrusClass.h>

#include <FDGE/Binding/Papyrus/ExtendedForm.h>
#include <FDGE/Binding/Papyrus/LocationExt.h>

using namespace FDGE::Binding::Papyrus;
using namespace RE;

namespace {
    PapyrusClass(LocationExt) {
        PapyrusStaticFunction(Extend, BGSLocation* location) -> ExtendedForm<LocationExt> {
            return {location};
        };

        PapyrusFunction(GetAllParents, ExtendedForm<LocationExt> self) {
            std::vector<ExtendedForm<LocationExt>> parents;
            for (BGSLocation* loc = self.GetForm()->parentLoc; loc; self = loc->parentLoc) {
                parents.emplace_back(loc);
            }
            return parents;
        };

        PapyrusFunction(GetChildLocations, BGSLocation* self, bool recursive) {
            std::vector<BGSLocation*> children;
            return children;
        };
    }
}
