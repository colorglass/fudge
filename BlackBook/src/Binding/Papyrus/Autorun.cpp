#include <articuno/archives/ryml/ryml.h>
#include <FDGE/Binding/Papyrus/ScriptLoader.h>
#include <FDGE/Binding/Papyrus/ScriptObject.h>
#include <FDGE/Hook/Serialization.h>
#include <FDGE/Plugin.h>
#include <FDGE/System.h>
#include <FDGE/Util/Maps.h>

using namespace articuno;
using namespace articuno::ryml;
using namespace FDGE;
using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Hook;
using namespace FDGE::Util;
using namespace gluino;
using namespace RE;
using namespace RE::BSScript;
using namespace RE::BSScript::Internal;

namespace {
    std::vector<std::string> AutorunScriptNames;

    class AutorunState {
    public:
        static inline AutorunState& GetSingleton() noexcept {
            static AutorunState instance;
            return instance;
        }

        istr_flat_hash_map<std::string, VMHandle> ActiveAutorunScripts;

    private:
        articuno_serde(ar) {
            ar <=> self(ActiveAutorunScripts);
        }

        friend class articuno::access;
    };

    class Autorun : public ScriptObject {
        ScriptType(Autorun);
    public:
        Autorun() {
            _self = this;
        }

    private:
        articuno_serde(ar) {
            ar <=> self(_self);
        }

        ScriptObjectHandle<Autorun> _self;

        friend class articuno::access;
    };

    void RunAutoruns() {
        auto* vm = VirtualMachine::GetSingleton();
        if (!vm) {
            return;
        }

        for (auto& scriptName : AutorunScriptNames) {
            if (AutorunState::GetSingleton().ActiveAutorunScripts.contains(scriptName)) {
                continue;
            }
            ScriptObjectHandle<Autorun> obj = (new Autorun())->Bind(scriptName);
            IFunctionArguments* args = MakeFunctionArguments();
            vm->SendEvent(obj->GetHandle(), "OnInit", args);
            AutorunState::GetSingleton().ActiveAutorunScripts.try_emplace(scriptName, obj->GetHandle());
        }
    }

    bool DiscoverAutoruns() {
        auto* vm = VirtualMachine::GetSingleton();
        if (!vm) {
            return false;
        }

        static std::atomic_bool initialized;
        static std::latch latch(1);
        if (initialized.exchange(true)) {
            latch.wait();
            return false;
        }

        ScriptLoader::LoadScripts("Data\\Scripts");
        std::vector<BSTSmartPointer<ObjectTypeInfo>> results;
        results.reserve(256);

        BSSpinLockGuard lock(vm->typeInfoLock);
        for (auto& entry : vm->objectTypeMap) {
            for (auto* typeInfo = entry.second->GetParent(); typeInfo != nullptr; typeInfo = typeInfo->GetParent()) {
                if (str_equal_to<false>{}(typeInfo->GetName(), "Autorun")) {
                    results.emplace_back(entry.second);
                    break;
                }
            }
        }

        std::sort(results.begin(), results.end(),
                  [](const BSTSmartPointer<ObjectTypeInfo>& a, BSTSmartPointer<ObjectTypeInfo>& b) {
                      return str_less<false>{}(a->GetName(), b->GetName());
                  });

        latch.count_down();
        return true;
    }

    OnSKSELoaded {
        System().ListenForever([](const NewGameEvent&) {
            DiscoverAutoruns();
        });

        System().ListenForever([](const SaveGameLoadedEvent&) {
            DiscoverAutoruns();
        });

        RegisterSaveHandler("Autorun", [](std::ostream& out) {
            yaml_sink ar(out);
            ar << AutorunState::GetSingleton().ActiveAutorunScripts;
        });
        RegisterLoadHandler("Autorun", [](std::istream& in) {
            yaml_source ar(in);
            ar >> AutorunState::GetSingleton().ActiveAutorunScripts;
        });
    }
}
