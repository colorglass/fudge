#include <FDGE/Binding/Papyrus/Any.h>
#include <FDGE/Binding/Papyrus/PapyrusClass.h>
#include <FDGE/Skyrim/ENB.h>

using namespace FDGE;
using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Skyrim;

namespace {
    class ENBProxy : public ScriptObject {
        ScriptType(ENB);

    public:

    private:
        articuno_serde() {}

        friend class articuno::access;
    };

    RegisterScriptType(ENBProxy);

    PapyrusClass(ENB) {
        PapyrusStaticFunction(GetENB) -> ENBProxy* {
            if (!ENB::GetSingleton().IsPresent()) {
                return nullptr;
            }
            return new ENBProxy();
        };

        PapyrusFunction(__IsPresent, ENBProxy*) {
            return ENB::GetSingleton().IsPresent();
        };

        PapyrusFunction(__IsEnabled, ENBProxy*) {
            return ENB::GetSingleton().IsEnabled();
        };

        PapyrusFunction(__SetEnabled, ENBProxy*, bool enabled) {
            return ENB::GetSingleton().SetEnabled(enabled);
        };

        PapyrusFunction(__GetRuntimeVersion, ENBProxy*) {
            return ENB::GetSingleton().GetVersion().value_or(0);
        };

        PapyrusFunction(__GetSDKVersion, ENBProxy*) {
            return ENB::GetSingleton().GetSDKVersion().value_or(0);
        };

        PapyrusFunction(__IsEditorActive, ENBProxy*) {
            return ENB::GetSingleton().IsEditorActive();
        };

        PapyrusFunction(__IsEffectsWindowActive, ENBProxy*) {
            return ENB::GetSingleton().IsEffectsWindowActive();
        };

        PapyrusFunction(__GetScreenWidth, ENBProxy*) {
            auto renderInfo = ENB::GetSingleton().GetRenderInfo().get_raw();
            return renderInfo ? renderInfo->GetScreenWidth() : 0;
        };

        PapyrusFunction(__GetScreenHeight, ENBProxy*) {
            auto renderInfo = ENB::GetSingleton().GetRenderInfo().get_raw();
            return renderInfo ? renderInfo->GetScreenHeight() : 0;
        };

        PapyrusFunction(__GetCursorXPosition, ENBProxy*) {
            return ENB::GetSingleton().GetCursorXPosition();
        };

        PapyrusFunction(__GetCursorYPosition, ENBProxy*) {
            return ENB::GetSingleton().GetCursorXPosition();
        };

        PapyrusFunction(__IsLeftMouseButtonClicked, ENBProxy*) {
            return ENB::GetSingleton().IsLeftMouseButtonPressed();
        };

        PapyrusFunction(__IsMiddleMouseButtonClicked, ENBProxy*) {
            return ENB::GetSingleton().IsMiddleMouseButtonPressed();
        };

        PapyrusFunction(__IsRightMouseButtonClicked, ENBProxy*) {
            return ENB::GetSingleton().IsRightMouseButtonPressed();
        };

        PapyrusFunction(GetParameter, ENBProxy*, std::string_view fileName, std::string_view category,
                        std::string_view key) -> Any* {
            if (fileName.empty() || category.empty() || key.empty()) {
                return new Any();
            }
            ENBParameter param;
            if (!ENB::GetSingleton().ReadParameter(fileName, category, key, param)) {
                return new Any();
            }
            switch (param.GetType()) {
                case ENBParameterType::enum_type::Boolean:
                    return new Any(param.GetBoolean().value_or(false));
                case ENBParameterType::enum_type::Float:
                    return new Any(param.GetFloat().value_or(0));
                case ENBParameterType::enum_type::Integer:
                    return new Any(param.GetInteger().value_or(0));
                default:
                    return new Any();
            }
        };

        PapyrusFunction(SetParameter, ENBProxy*, std::string_view fileName, std::string_view category,
                        std::string_view key, Any* value) {
            if (fileName.empty() || category.empty() || key.empty() || !value) {
                return false;
            }
            return std::visit([&](auto&& value) {
                using value_type = std::remove_cvref_t<decltype(value)>;

                if constexpr (std::same_as<value_type, bool>) {
                    return ENB::GetSingleton().WriteParameter(fileName, category, key, ENBParameter(value));
                } else if constexpr (std::same_as<value_type, int32_t>) {
                    return ENB::GetSingleton().WriteParameter(fileName, category, key, ENBParameter(value));
                } else if constexpr (std::same_as<value_type, float_t>) {
                    return ENB::GetSingleton().WriteParameter(fileName, category, key, ENBParameter(value));
                } else {
                    return false;
                }
            }, value->GetValue());
        };
    }
}
