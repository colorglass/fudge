#include <FDGE/Binding/Papyrus/PriorityQueue.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

int32_t PriorityQueue::GetSize() const noexcept {
    return static_cast<int32_t>(_value.size());
}

void PriorityQueue::Enqueue(int32_t priority, Any* value) {
    std::unique_lock lock(_lock);
    _value.emplace(priority, value);
}

Any* PriorityQueue::Dequeue() noexcept {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return nullptr;
    }
    auto* result = const_cast<Any*>(_value.top().Value.GetObject());
    _value.pop();
    return result;
}

Any* PriorityQueue::Peek() noexcept {
    std::unique_lock lock(_lock);
    if (_value.empty()) {
        return nullptr;
    }
    return const_cast<Any*>(_value.top().Value.GetObject());
}

namespace {
    RegisterScriptType(PriorityQueue)

    PapyrusClass(PriorityQueue) {
        PapyrusStaticFunction(Create) {
            return new PriorityQueue();
        };

        PapyrusFunction(__GetSize, PriorityQueue* self) {
            return self ? self->GetSize() : 0;
        };

        PapyrusFunction(Enqueue, PriorityQueue* self, int32_t priority, Any* value) {
            if (self) {
                self->Enqueue(priority, value);
            }
        };

        PapyrusFunction(Dequeue, PriorityQueue* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->Dequeue();
        };

        PapyrusFunction(Peek, PriorityQueue* self) -> Any* {
            if (!self) {
                return nullptr;
            }
            return self->Peek();
        };
    }
}
