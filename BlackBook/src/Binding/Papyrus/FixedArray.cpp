#include <FDGE/Binding/Papyrus/FixedArray.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding::Papyrus;

FixedArray::FixedArray(int32_t length) {
    _value.resize(length);
}

FixedArray::FixedArray(FixedArray* original, int32_t length) {
    _value.resize(length);
    for (std::size_t i = 0; i < original->GetSize() && i < length; ++i) {
        _value[i] = original->_value[i];
    }
}

ScriptObjectHandle<Any> FixedArray::GetValue(std::int32_t index) noexcept {
    std::unique_lock lock(_lock);
    if (index < 0) {
        index = GetSize() + index;
    }
    if (index < 0 || index >= GetSize()) {
        return {};
    }
    return _value[index];
}

bool FixedArray::SetValue(std::int32_t index, Any* value) noexcept {
    std::unique_lock lock(_lock);
    if (index < 0) {
        index = GetSize() + index;
    }
    if (index < 0 || index >= GetSize()) {
        return false;
    }
    _value[index] = value;
    ++_version;
    return true;
}

Any* FixedArray::Erase(int32_t index) noexcept {
    std::unique_lock lock(_lock);
    if (index < 0) {
        index = GetSize() + index;
    }
    if (index < 0 || index >= GetSize()) {
        return nullptr;
    }
    auto result = _value[index];
    _value.erase(_value.begin() + index);
    ++_version;
    return result;
}

int32_t FixedArray::GetSize() const noexcept {
    return static_cast<int32_t>(_value.size());
}

bool FixedArray::Clear() noexcept {
    std::unique_lock lock(_lock);
    if (!_value.empty()) {
        _value.clear();
        ++_version;
        return true;
    }
    return false;
}

bool FixedArray::Contains(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    for (auto& entry : _value) {
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                return true;
            }
        } else if (value->Equals(candidate)) {
            return true;
        }
    }
    return false;
}

int32_t FixedArray::Count(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    int32_t result = 0;
    for (auto& entry : _value) {
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                ++result;
            }
        } else if (value->Equals(candidate)) {
            ++result;
        }
    }
    return result;
}

int32_t FixedArray::IndexOf(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    int32_t index = 0;
    for (auto& entry : _value) {
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                return index;
            }
        } else if (value->Equals(candidate)) {
            return index;
        }
        ++index;
    }
    return -1;
}

int32_t FixedArray::ReverseIndexOf(Any* value) const noexcept {
    std::unique_lock lock(_lock);
    for (int32_t index = static_cast<int32_t>(_value.size()) - 1; index >= 0; --index) {
        auto& entry = _value[index];
        auto* candidate = entry.GetObject();
        if (!value) {
            if (!candidate) {
                return index;
            }
        } else if (value->Equals(candidate)) {
            return index;
        }
    }
    return -1;
}

ValueIterator* FixedArray::GetValueIterator() noexcept {
    return new FixedArrayIterator(this, false);
}

ValueIterator* FixedArray::GetReverseValueIterator() noexcept {
    return new FixedArrayIterator(this, true);
}

ValueIterator* FixedArray::GetValueIteratorFrom(int32_t index) noexcept {
    return new FixedArrayIterator(this, false, index);
}

ValueIterator* FixedArray::GetReverseValueIteratorFrom(int32_t index) noexcept {
    return new FixedArrayIterator(this, true, index);
}

FixedArrayIterator::FixedArrayIterator(ScriptObjectHandle<FixedArray> parent, bool reverse) noexcept
        : _parent(std::move(parent)), _reverse(reverse) {
    FixedArray* array = _parent;
    std::unique_lock lock(array->_lock);
    _version = array->_version;
    if (_reverse) {
        _index = array->GetSize() - 1;
    }
    if (IsValid()) {
        _value = array->GetValue(static_cast<int32_t>(_index));
    }
}

FixedArrayIterator::FixedArrayIterator(ScriptObjectHandle<FixedArray> parent, bool reverse,
                                           int32_t index) noexcept : _parent(std::move(parent)), _reverse(reverse) {
    FixedArray* array = _parent;
    std::unique_lock lock(array->_lock);
    if (index < 0) {
        index = array->GetSize() + index;
    }
    if (_reverse) {
        index = array->GetSize() - index - 1;
    }
    _index = index;
    _version = array->_version;
    if (IsValid()) {
        _value = array->GetValue(static_cast<int32_t>(_index));
    }
}

bool FixedArrayIterator::IsValid() noexcept {
    FixedArray* array = _parent;
    return array->GetVersion() == _version && _index < array->GetSize() && _index >= 0;
}

bool FixedArrayIterator::HasMore() noexcept {
    FixedArray* array = _parent;
    return _index < array->GetSize() && _index >= 0;
}

Any* FixedArrayIterator::GetValue() noexcept {
    return _value.GetObject();
}

bool FixedArrayIterator::SetValue(Any* value) noexcept {
    FixedArray* array = _parent;
    std::unique_lock lock(array->_lock);
    if (!IsValid()) {
        return false;
    }
    _version = ++array->_version;
    array->_value[_index] = value;
    return true;
}

bool FixedArrayIterator::Next() noexcept {
    FixedArray* array = _parent;
    std::unique_lock lock(array->_lock);
    _reverse ? --_index : ++_index;
    if (!IsValid()) {
        _value = {};
        return false;
    }
    _value = array->GetValue(static_cast<int32_t>(_index));
    return true;
}

namespace {
    RegisterScriptType(FixedArray);
    RegisterScriptType(FixedArrayIterator);

    PapyrusClass(FixedArray) {
        PapyrusStaticFunction(Create, int32_t length) -> FixedArray* {
            if (length < 0) {
                return nullptr;
            }
            return new FixedArray(length);
        };

        PapyrusFunction(Resize, FixedArray* self, int32_t size) -> FixedArray* {
            if (!self || size < 0) {
                return nullptr;
            }
            return new FixedArray(self, size);
        };
    };
}
