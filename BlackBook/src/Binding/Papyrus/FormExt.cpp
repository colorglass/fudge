#pragma once

#include <FDGE/Binding/Papyrus/ExtendedForm.h>
#include <FDGE/Binding/Papyrus/PapyrusClass.h>
#include <FDGE/Skyrim/FormIndex.h>

using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Skyrim;
using namespace RE;
using namespace RE::BSScript;

namespace {
    PapyrusClass(FormExt) {
        PapyrusStaticFunction(GetFormObject, TESForm* form,
                              std::string_view className) -> AttachedObjectHandle<TESForm> {
            if (!form || className.empty()) {
                return {};
            }
            VMHandle handle = 0xFFFF00000000 | form->GetFormID();
            BSSpinLockGuard lock(VirtualMachine->attachedScriptsLock);
            auto result = VirtualMachine->attachedScripts.find(handle);
            if (result == VirtualMachine->attachedScripts.end()) {
                return {};
            }
            BSFixedString iClassName = className;
            for (auto& attachedScript : result->second) {
                if (iClassName == attachedScript->GetTypeInfo()->GetName()) {
                    return BSTSmartPointer<Object>(attachedScript.get());
                }
            }
            return {};
        };

        PapyrusStaticFunction(GetEditorID, TESForm* form) -> std::string_view {
            if (!form) {
                return "";
            }
            return form->GetFormEditorID();
        };

        PapyrusStaticFunction(GetFormByEditorID, std::string_view editorID) -> TESForm* {
            if (editorID.empty()) {
                return nullptr;
            }
            FormIndex index;
            return index.Lookup(editorID).get_raw();
        };

        PapyrusStaticFunction(GetFormByPortableID, std::string_view portableID) -> TESForm* {
            if (portableID.empty()) {
                return nullptr;
            }
            FormIndex index;
            try {
                return index.Lookup(PortableID(portableID)).get_raw();
            } catch (...) {
                return nullptr;
            }
        };
    }
}
