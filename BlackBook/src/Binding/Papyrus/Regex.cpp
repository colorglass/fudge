#include <FDGE/Binding/Papyrus/Regex.h>

#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE;
using namespace FDGE::Binding::Papyrus;
using namespace gluino;
using namespace RE;
using namespace srell;

BSFixedString Regex::ToString() const noexcept {
    return GetPattern().data();
}

bool Regex::Equals(const ScriptObject* other) const noexcept {
    if (!other) {
        return false;
    }
    auto* regex = dynamic_cast<const Regex*>(other);
    if (!regex) {
        return false;
    }
    return _pattern == regex->_pattern && _regex.flags() == regex->_regex.flags();
}

std::size_t Regex::GetHashCode() const noexcept {
    auto seed = str_hash<false>{}(_pattern);
    seed ^= static_cast<uint64_t>(_regex.flags()) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    return seed;
}

BSFixedString MatchResult::ToString() const noexcept {
    return _value.c_str();
}

bool MatchResult::Equals(const ScriptObject* other) const noexcept {
    if (!other) {
        return false;
    }
    auto* match = dynamic_cast<const MatchResult*>(other);
    if (!match) {
        return false;
    }
    return _value == match->_value && _startIndex == match->_startIndex;
}

std::size_t MatchResult::GetHashCode() const noexcept {
    auto seed = str_hash<false>{}(_value);
    seed ^= _startIndex + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    return seed;
}

namespace {
    RegisterScriptType(Regex);
    RegisterScriptType(MatchResult);

    PapyrusClass(Regex) {
        PapyrusStaticFunction(Create, std::string_view pattern, bool optimize) noexcept -> Regex* {
            auto flags = static_cast<regex::flag_type>(regex::flag_type::ECMAScript);
            flags |= regex::icase;
            if (optimize) {
                flags |= regex::optimize;
            }
            try {
                return new Regex(pattern, flags);
            } catch (const regex_error&) {
                return nullptr;
            }
        };

        PapyrusFunction(__GetPattern, Regex* self) noexcept -> std::string_view {
            if (!self) {
                return "";
            }
            return self->GetPattern();
        };

        PapyrusFunction(__IsOptimized, Regex* self) noexcept {
            if (!self) {
                return false;
            }
            return static_cast<bool>(self->GetFlags() & regex::optimize);
        };

        PapyrusFunction(Match, Regex* self, std::string_view value) -> std::vector<MatchResult*> {
            std::vector<MatchResult*> results;
            if (!self) {
                return results;
            }
            cmatch matches;
            if (regex_search(value.data(), matches, self->GetCompiled())) {
                for (std::size_t i = 0; i < matches.size(); ++i) {
                    results.emplace_back(new MatchResult(matches.str(i), static_cast<int32_t>(matches.position(i))));
                }
            }
            return results;
        };

        PapyrusFunction(ExactMatch, Regex* self, std::string_view value) -> std::vector<MatchResult*> {
            std::vector<MatchResult*> results;
            if (!self) {
                return results;
            }
            cmatch matches;
            if (regex_match(value.data(), matches, self->GetCompiled())) {
                for (std::size_t i = 0; i < matches.size(); ++i) {
                    results.emplace_back(new MatchResult(matches.str(i), static_cast<int32_t>(matches.position(i))));
                }
            }
            return results;
        };

        PapyrusFunction(IsMatch, Regex* self, std::string_view value) {
            if (!self) {
                return false;
            }
            return regex_search(value.data(), self->GetCompiled());
        };

        PapyrusFunction(IsExactMatch, Regex* self, std::string_view value) {
            if (!self) {
                return false;
            }
            return regex_match(value.data(), self->GetCompiled());
        };

        PapyrusFunction(Replace, Regex* self, std::string_view value,
                        std::string_view replacement, bool firstOnly, bool noCopy) -> std::string {
            if (!self) {
                return "";
            }
            regex_constants::match_flag_type flags = regex_constants::match_flag_type::format_default;
            if (firstOnly) {
                flags |= regex_constants::match_flag_type::format_first_only;
            }
            if (noCopy) {
                flags |= regex_constants::match_flag_type::format_no_copy;
            }
            return regex_replace(value.data(), self->GetCompiled(), replacement.data(), flags);
        };
    };

    PapyrusClass(MatchResult) {
        PapyrusFunction(__GetValue, MatchResult* self) {
            if (!self) {
                return std::string_view("");
            }
            return self->GetValue();
        };

        PapyrusFunction(__GetStartIndex, MatchResult* self) {
            if (!self) {
                return 0;
            }
            return self->GetStartIndex();
        };

        PapyrusFunction(__GetEndIndex, MatchResult* self) {
            if (!self) {
                return 0;
            }
            return static_cast<int32_t>(self->GetStartIndex() + self->GetValue().size());
        };

        PapyrusFunction(__GetMatchLength, MatchResult* self) {
            if (!self) {
                return 0;
            }
            return static_cast<int32_t>(self->GetValue().size());
        };
    };
}
