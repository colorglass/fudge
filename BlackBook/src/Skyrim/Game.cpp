#include <FDGE/Skyrim/Game.h>

using namespace FDGE;
using namespace FDGE::Skyrim;

namespace FDGE::Skyrim::detail {
    class GameManager {
    public:
        [[nodiscard]] inline static Game CreateGame() noexcept {
            return Game();
        }
    };
}

const FDGE::Skyrim::detail::Game FDGE::Skyrim::Game = FDGE::Skyrim::detail::GameManager::CreateGame();
