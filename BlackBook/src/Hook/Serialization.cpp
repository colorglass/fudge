#include <FDGE/Hook/Serialization.h>

#include <articuno/articuno.h>
#include <articuno/archives/ryml/ryml.h>
#include <articuno/types/auto.h>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/zstd.hpp>
#include <FDGE/Logger.h>
#include <FDGE/System.h>
#include <FDGE/Util/Maps.h>
#include <gluino/autorun.h>
#include <RE/Skyrim.h>
#include <winbase.h>

#include "../FDGEConfig.h"

using namespace articuno;
using namespace articuno::ryml;
using namespace boost::iostreams;;
using namespace FDGE;
using namespace FDGE::Hook;
using namespace RE;

namespace {
    constexpr std::string_view PreSuffix = ".pre.fudge";
    constexpr std::string_view PostSuffix = ".post.fudge";

    std::filesystem::path SavePath;

    gluino_autorun {
        SavePath = "My Games";
        SavePath /= "Skyrim Special Edition";
    }

    std::mutex CallbacksLock;
    Util::istr_flat_hash_map<std::string,
            Util::istr_flat_hash_map<std::string, std::function<void(std::ostream&)>>> PreSaveCallbacks;
    Util::istr_flat_hash_map<std::string,
            Util::istr_flat_hash_map<std::string, std::function<void(std::ostream&)>>> PostSaveCallbacks;
    Util::istr_flat_hash_map<std::string,
            Util::istr_flat_hash_map<std::string, std::function<void(std::istream&)>>> PreLoadCallbacks;
    Util::istr_flat_hash_map<std::string,
            Util::istr_flat_hash_map<std::string, std::function<void(std::istream&)>>> PostLoadCallbacks;

    template <class T>
    class Cosave {
    public:
        inline explicit Cosave(const T& callbacks) noexcept : _callbacks(callbacks) {
        }

    private:
        template <class U>
        struct CosaveModule {
            inline explicit CosaveModule(const U& callbacks) noexcept : _callbacks(callbacks) {
            }

            articuno_serialize(ar) {
                for (auto& entry : _callbacks) {
                    std::stringstream buffer;
                    entry.second(buffer);
                    if (buffer.tellp()) {
                        ar <=> kv(buffer, entry.first);
                    } else {
                        ar <=> null();
                    }
                }
            }

            articuno_deserialize(ar) {
                for (auto& key : ar) {
                    auto result = _callbacks.find(key.second);
                    if (result == _callbacks.end()) {
                        Logger::Warn("Found co-save handler '{}' but no matching handler was found; mod was possibly "
                                     "uninstalled. Resaving will likely result in this data being lost.", key.second);
                        continue;
                    }
                    std::stringstream buffer;
                    ar <=> kv(buffer, key.second);
                    result->second(buffer);
                }
            }

            const U& _callbacks;
        };

        articuno_serialize(ar) {
            for (auto& module : _callbacks) {
                CosaveModule mod(module.second);
                ar <=> kv(mod, module.first);
            }
        }

        articuno_deserialize(ar) {
            for (auto& key: ar) {
                auto result = _callbacks.find(key.second);
                if (result == _callbacks.end()) {
                    Logger::Warn("Found co-save entry '{}' but no matching mod was found; mod was possibly "
                                 "uninstalled. Resaving will likely result in this data being lost.", key.second);
                    continue;
                }
                CosaveModule mod(result->second);
                ar <=> kv(mod, key.second);
            }
        }

        const T& _callbacks;

        friend class articuno::access;
    };

    std::filesystem::path GetSavePath(const std::filesystem::path& saveName) {
        FDGE_WIN_CHAR_TYPE myDocumentsPath[MAX_PATH];
        auto result = SHGetFolderPath(nullptr, CSIDL_PERSONAL, nullptr, SHGFP_TYPE_CURRENT, myDocumentsPath);
        if (result != S_OK) {
            Logger::Error("Unable to determine documents path.");
            throw std::runtime_error("Unable to determine documents path.");
        }
        std::filesystem::path savePath(myDocumentsPath);
        savePath /= SavePath;

        // Default save location is under Saves directory, but this can be overridden in the Skyrim INI files.
        auto* savePathSetting = GameSettingCollection::GetSingleton()->GetSetting("sLocalSavePath:General");
        if (savePathSetting && (savePathSetting->GetType() == Setting::Type::kString) &&
            savePathSetting->GetString()[0] != '\0') {
            savePath /= savePathSetting->GetString();
        } else {
            savePath /= "Saves";
        }

        savePath /= saveName;
        return savePath;
    }

    void HandleSaveEvent(std::filesystem::path savePath, std::string_view extension, const auto& callbacks) {
        std::unique_lock lock(CallbacksLock);
        savePath += extension;
        if (callbacks.empty()) {
            Logger::Debug("No save handlers registered, skipping co-save generation for '{}'.", savePath.string());
            return;
        }

        auto format = FDGEConfig::GetProxy()->GetCosave().GetFormat();
        if (format == CosaveFormat::Auto) {
            // TODO: When Boost fixes their Zstd filter, make Zstd the default.
            format = CosaveFormat::YAMLWithZlib;
        }

        Cosave cosave(callbacks);
        std::stringstream buffer;
        yaml_sink<> ar(buffer);
        ar << cosave;
        if (buffer.tellp()) {
            std::fstream file(savePath, std::fstream::out);
            file.write(reinterpret_cast<char*>(&format), sizeof(format));
            filtering_ostream out;
            if (format == CosaveFormat::YAMLWithZlib) {
                out.push(zlib_compressor());
            } else if (format == CosaveFormat::YAMLWithZstd) {
                out.push(zstd_compressor());
            }
            out.push(file);

            char readBuffer[4096];
            while (!buffer.eof()) {
                buffer.read(readBuffer, sizeof(readBuffer));
                out.write(readBuffer, buffer.gcount());
            }

            out.flush();
        }
    }

    void HandleLoadEvent(std::filesystem::path savePath, std::string_view extension, const auto& callbacks) {
        std::unique_lock lock(CallbacksLock);
        savePath += extension;
        if (!std::filesystem::exists(savePath)) {
            Logger::Debug("No co-save file '{}' found, skipping loading.", savePath.string());
            return;
        }

        Cosave cosave(callbacks);
        std::fstream file(savePath, std::fstream::in);
        if (file.eof()) {
            Logger::Info("Found an empty co-save file '{}', skipping loading.", savePath.string());
            return;
        }

        CosaveFormat format;
        file.read(reinterpret_cast<char*>(&format), sizeof(format));

        boost::iostreams::filtering_istream in;
        if (format == CosaveFormat::YAMLWithZlib) {
            in.push(zlib_decompressor());
        } else if (format == CosaveFormat::YAMLWithZstd) {
            in.push(zstd_decompressor());
        }
        in.push(file);

        yaml_source<> ar(in);
        ar >> cosave;
    }

    OnSKSELoaded {
        System().ListenForever([](const GameSavingEvent& event) {
            auto baseSavePath = GetSavePath(event.GetSaveName());
            HandleSaveEvent(baseSavePath, PreSuffix, PreSaveCallbacks);
            HandleSaveEvent(baseSavePath, PostSuffix, PostSaveCallbacks);
        });

        System().ListenForever([](const SaveGameLoadingEvent& event) {
            auto baseSavePath = GetSavePath(event.GetSaveName());
            HandleLoadEvent(baseSavePath, PreSuffix, PreLoadCallbacks);
        });

        System().ListenForever([](const SaveGameLoadedEvent& event) {
            auto baseSavePath = GetSavePath(event.GetSaveName());
            HandleLoadEvent(baseSavePath, PostSuffix, PostLoadCallbacks);
        });

        System().ListenForever([](const SaveGameDeletingEvent& event) {
            auto preSavePath = GetSavePath(event.GetSaveName());
            preSavePath += PreSuffix;
            auto postSavePath = GetSavePath(event.GetSaveName());
            postSavePath += PostSuffix;

            auto result = std::filesystem::remove(preSavePath);
            if (result) {
                Logger::Debug("Deleted FuDGE pre-cosave file '{}'.", preSavePath.string());
            } else {
                Logger::Debug("No FuDGE pre-cosave file '{}' found, deletion not required.", preSavePath.string());
            }

            result = std::filesystem::remove(postSavePath);
            if (result) {
                Logger::Debug("Deleted FuDGE post-cosave file '{}'.", postSavePath.string());
            } else {
                Logger::Debug("No FuDGE post-cosave file '{}' found, deletion not required.", postSavePath.string());
            }
        });
    }

    void ValidateRegistration(std::string_view moduleName, std::string_view handlerName, HandlerStage stage) {
        if (moduleName.empty()) {
            Logger::Error("Module name for serialization handler cannot be empty.");
            throw std::invalid_argument("Module name for serialization handler cannot be empty.");
        }
        if (handlerName.empty()) {
            Logger::Error("Handler name for serialization handler cannot be empty.");
            throw std::invalid_argument("Handler name for serialization handler cannot be empty.");
        }
        auto stageValue = static_cast<typename HandlerStage::underlying_type>(stage);
        if (stageValue > 1) {
            Logger::Error("Invalid handler stage value {}.", stageValue);
            throw std::invalid_argument(fmt::format("Invalid handler stage value {}.", stageValue));
        }
    }

    inline bool Unregister(std::string_view moduleName, std::string_view handlerName, auto& callbacks) {
        auto result = callbacks.find(moduleName);
        if (result == callbacks.end()) {
            return false;
        }
        return result->second.erase(handlerName);
    }
}

void FDGE::Hook::detail::RegisterSaveHandler(
        std::string_view moduleName, std::string_view handlerName, HandlerStage stage,
        const std::function<void(std::ostream&)>& handler) {
    ValidateRegistration(moduleName, handlerName, stage);
    std::unique_lock lock(CallbacksLock);
    if (stage == HandlerStage::Pre) {
        PreSaveCallbacks[moduleName][handlerName] = handler;
    } else {
        PostSaveCallbacks[moduleName][handlerName] = handler;
    }
}

void FDGE::Hook::detail::RegisterLoadHandler(
        const std::vector<std::string>& moduleNames, std::string_view handlerName, HandlerStage stage,
        const std::function<void(std::istream&)>& handler) {
    for (auto& moduleName : moduleNames) {
        ValidateRegistration(moduleName, handlerName, stage);
    }
    std::unique_lock lock(CallbacksLock);
    if (stage == HandlerStage::Pre) {
        for (auto& moduleName : moduleNames) {
            PreLoadCallbacks[moduleName][handlerName] = handler;
        }
    } else {
        for (auto& moduleName : moduleNames) {
            PostLoadCallbacks[moduleName][handlerName] = handler;
        }
    }
}

bool FDGE::Hook::detail::UnregisterSaveHandler(std::string_view moduleName, std::string_view handlerName,
                                                        HandlerStage stage) {
    ValidateRegistration(moduleName, handlerName, stage);
    std::unique_lock lock(CallbacksLock);
    if (stage == HandlerStage::Pre) {
        return Unregister(moduleName, handlerName, PreSaveCallbacks);
    } else {
        return Unregister(moduleName, handlerName, PostSaveCallbacks);
    }
}

bool FDGE::Hook::detail::UnregisterLoadHandler(const std::vector<std::string>& moduleNames,
                                                        std::string_view handlerName, HandlerStage stage) {
    for (auto& moduleName : moduleNames) {
        ValidateRegistration(moduleName, handlerName, stage);
    }
    bool success = true;
    std::unique_lock lock(CallbacksLock);
    if (stage == HandlerStage::Pre) {
        for (auto& moduleName : moduleNames) {
            success &= Unregister(moduleName, handlerName, PreLoadCallbacks);
        }
    } else {
        for (auto& moduleName : moduleNames) {
            success &= Unregister(moduleName, handlerName, PostLoadCallbacks);
        }
    }
    return success;
}
