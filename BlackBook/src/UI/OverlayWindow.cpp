#include <FDGE/UI/OverlayWindow.h>

#include <FDGE/Logger.h>
#include <FDGE/Plugin.h>
#include <QtQuick/QQuickWindow>

#include "../FDGEConfig.h"

using namespace FDGE;
using namespace FDGE::UI;
using namespace RE;
using namespace SKSE;

OverlayWindow::OverlayWindow(const QQuickRenderTarget& renderTarget) {
    _window = new QQuickWindow();
    _window->setRenderTarget(renderTarget);
    _window->setFlags(Qt::FramelessWindowHint | Qt::SubWindow | Qt::WindowStaysOnTopHint);
    _window->setOpacity(0.00);
}

LayerHandle OverlayWindow::AddLayer(OverlayLayer* layer) {
    LayerHandle result;
    if (!layer) {
        return {};
    }
    QTimer::singleShot(0, [&]() {
        layer->setParent(_window->contentItem());
    });
    return {this, layer};
}

bool OverlayWindow::RemoveLayer(OverlayLayer* layer) {
    bool result = false;
    std::latch waiter(1);

    QTimer::singleShot(0, [&]() {
        for (auto* child : _window->children()) {
            if (child == layer) {
                layer->setParent(nullptr);
                result = true;
                break;
            }
        }
        waiter.count_down();
    });

    waiter.wait();
    return result;
}
