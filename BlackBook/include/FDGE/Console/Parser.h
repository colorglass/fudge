#pragma once

#include <FDGE/Util/Maps.h>

namespace FDGE::Console {
    namespace detail {
        class CommandGrammar;
    }

#pragma warning(push)
#pragma warning(disable: 4251)
    class __declspec(dllexport) Parser {
    public:
        inline Parser() noexcept = default;

        explicit Parser(std::string_view input);

        ~Parser();

        [[nodiscard]] inline bool IsValid() const noexcept {
            return _valid;
        }

        [[nodiscard]] std::string_view GetExplicitTarget() const noexcept;

        [[nodiscard]] inline bool IsExplicitlyNonTargeting() const noexcept {
            return _explicitlyNonTargeting;
        }

        [[nodiscard]] std::string_view GetNamespace() const noexcept;

        [[nodiscard]] std::string_view GetCommand() const noexcept;

        [[nodiscard]] std::string_view GetParameter(std::string_view fullName) const;

        [[nodiscard]] bool HasFlag(std::string_view fullName, std::string_view shortName = "") const;

        bool NextPositionalArgument(std::string_view& result) const noexcept;

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_valid, "valid");
            ar <=> articuno::kv(_explicitlyNonTargeting, "explicitlyNonTargeting");
            ar <=> articuno::kv(_explicitTarget, "explicitTarget");
            ar <=> articuno::kv(_namespace, "namespace");
            ar <=> articuno::kv(_command, "command");
            ar <=> articuno::kv(_parameters, "parameters");
            ar <=> articuno::kv(_flags, "flags");
            ar <=> articuno::kv(_shortFlags, "shortFlags");
            ar <=> articuno::kv(_positionalArguments, "positionalArguments");
            ar <=> articuno::kv(_position, "position");
        }

        bool _valid{false};
        bool _explicitlyNonTargeting{false};
        std::string _explicitTarget;
        std::string _namespace;
        std::string _command;
        FDGE::Util::istr_flat_hash_map<std::string, std::string> _parameters{};
        FDGE::Util::istr_flat_hash_set<std::string> _flags{};
        FDGE::Util::istr_flat_hash_set<std::string> _shortFlags{};
        std::deque<std::string> _positionalArguments{};
        mutable std::size_t _position{0};

        friend class FDGE::Console::detail::CommandGrammar;

        friend class articuno::access;
    };

#pragma warning(pop)
}
