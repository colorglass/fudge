#pragma once

#include "Any.h"

namespace FDGE::Binding::Papyrus {
    class Stack : public ScriptObject {
        ScriptType(Stack)

    public:
        [[nodiscard]] int32_t GetSize() const noexcept;

        void Push(Any* value);

        Any* Pop() noexcept;

        Any* Peek() noexcept;

    private:
        articuno_serde(ar) {
            ar <=> articuno::self(_value);
        }

        mutable std::mutex _lock;
        std::stack<ScriptObjectHandle<Any>> _value;

        friend class articuno::access;
    };
}
