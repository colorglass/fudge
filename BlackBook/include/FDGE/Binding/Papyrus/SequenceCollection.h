#pragma once

#include "Collection.h"

namespace FDGE::Binding::Papyrus {
    class SequenceCollection : public Collection {
    ScriptType(SequenceCollection)

    public:
        [[nodiscard]] virtual bool Contains(Any* value) const noexcept = 0;

        [[nodiscard]] virtual int32_t Count(Any* value) const noexcept = 0;

        [[nodiscard]] virtual int32_t IndexOf(Any* value) const noexcept = 0;

    protected:
        inline SequenceCollection() noexcept = default;

    private:
        articuno_serde() {}

        friend class articuno::access;
    };
}
