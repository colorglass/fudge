#pragma once

#include <RE/Skyrim.h>

namespace FDGE::Binding::Papyrus {
    struct LocationExt {
        using form_type = RE::BGSLocation;

        static constexpr std::string_view TypeName = "LocationExt";
    };
}
