#pragma once

#include "ScriptObject.h"
#include "../../Console/CommandRepository.h"
#include "../../Console/Execution.h"

namespace FDGE::Binding::Papyrus {
    class ConsoleCommandExecution : public ScriptObject, public FDGE::Console::Execution {
        ScriptType(ConsoleCommandExecution);

    public:
        inline ConsoleCommandExecution() noexcept = default;

        ConsoleCommandExecution(FDGE::Console::Execution execution) noexcept;

    private:
        articuno_serde(ar, vflags) {
            articuno_super(FDGE::Console::Execution, ar, vflags);
        }

        friend class articuno::access;
    };

    class ConsoleCommand : public ScriptObject {
        ScriptType(ConsoleCommand);

    public:
        inline ConsoleCommand() noexcept = default;

        ConsoleCommand(std::string_view ns, std::string_view name, std::string_view shortName);

        [[nodiscard]] inline std::string_view GetNamespace() const noexcept {
            return _namespace;
        }

        [[nodiscard]] inline std::string_view GetName() const noexcept {
            return _name;
        }

        [[nodiscard]] inline std::string_view GetShortName() const noexcept {
            return _shortName;
        }

        [[nodiscard]] inline RE::VMHandle GetTarget() const noexcept {
            return _target;
        }

        bool Register(RE::VMHandle handle);

        bool Unregister(RE::VMHandle handle);

    private:
        articuno_serialize(ar) {
            ar <=> articuno::kv(_namespace, "namespace");
            ar <=> articuno::kv(_name, "name");
            ar <=> articuno::kv(_shortName, "shortName");
            ar <=> articuno::kv(_target, "target");
        }

        articuno_deserialize(ar) {
            ar <=> articuno::kv(_namespace, "namespace");
            ar <=> articuno::kv(_name, "name");
            ar <=> articuno::kv(_shortName, "shortName");

            RE::VMHandle target;
            ar <=> articuno::kv(target, "target");
            if (target) {
                Register(target);
            }
        }

        void HandleCommand(const FDGE::Console::Execution& execution);

        std::string _namespace;
        std::string _name;
        std::string _shortName;
        std::atomic<RE::VMHandle> _target{0};
        FDGE::Console::CommandRegistration _registration;

        friend class articuno::access;
    };
}
