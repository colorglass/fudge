#pragma once

#include <tsl/ordered_set.h>

#include "SetCollection.h"

namespace FDGE::Binding::Papyrus {
    class HashSetIterator;

    class HashSet : public SetCollection {
    ScriptType(HashSet);

    public:
        inline HashSet() noexcept = default;

        [[nodiscard]] Any* GetValue(Any* key) noexcept override;

        [[nodiscard]] Any* PutValue(Any* value) noexcept override;

        [[nodiscard]] bool TryPutValue(Any* value) noexcept override;

        [[nodiscard]] Any* Delete(Any* value) noexcept override;

        [[nodiscard]] int32_t GetSize() const noexcept override;

        [[nodiscard]] bool Clear() noexcept override;

        [[nodiscard]] bool Contains(Any* value) const noexcept override;

        [[nodiscard]] ValueIterator* GetValueIterator() noexcept override;

        [[nodiscard]] ValueIterator* GetReverseValueIterator() noexcept;

        [[nodiscard]] ValueIterator* GetValueIteratorFrom(Any* value) noexcept override;

        [[nodiscard]] ValueIterator* GetReverseValueIteratorFrom(Any* value) noexcept;

        [[nodiscard]] inline uint64_t GetVersion() const noexcept {
            return _version;
        }

        void Reserve(int32_t capacity);

    private:
        using AnyType = ScriptObjectHandle<Any>;

        articuno_serde(ar) {
            ar <=> articuno::kv(_value, "value");
            ar <=> articuno::kv(_version, "version");
        }

        mutable std::recursive_mutex _lock;
        tsl::ordered_set<AnyType, std::hash<AnyType>, std::equal_to<AnyType>> _value;
        uint64_t _version{0};

        friend class HashSetIterator;
        friend class articuno::access;
    };

    class HashSetIterator : public ValueIterator {
    ScriptType(HashSetIterator);

    public:
        HashSetIterator(HashSet* parent, bool reverse) noexcept;

        HashSetIterator(HashSet* parent, bool reverse, Any* key) noexcept;

        [[nodiscard]] bool IsValid() noexcept override;

        [[nodiscard]] bool HasMore() noexcept override;

        [[nodiscard]] Any* GetValue() noexcept override;

        bool SetValue(Any* value) noexcept override;

        [[nodiscard]] bool Next() noexcept override;

    protected:
        void OnGameLoadComplete() override;

    private:
        inline HashSetIterator() noexcept = default;

        articuno_serde(ar) {
            ar <=> articuno::kv(_parent, "parent");
            ar <=> articuno::kv(_version, "version");
            ar <=> articuno::kv(_reverse, "reverse");
            ar <=> articuno::kv(_value, "value");
            ar <=> articuno::kv(_beforeBegin, "beforeBegin");
        }

        ScriptObjectHandle<HashSet> _parent;
        uint64_t _version{0};
        bool _reverse{false};
        bool _beforeBegin{false};
        ScriptObjectHandle<Any> _value;
        typename decltype(HashSet::_value)::iterator _iter;

        friend class articuno::access;
    };
}
