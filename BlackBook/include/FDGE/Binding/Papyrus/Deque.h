#pragma once

#include "RandomAccessCollection.h"

namespace FDGE::Binding::Papyrus {
    class DequeIterator;

    class Deque : public RandomAccessCollection {
        ScriptType(Deque);

    public:
        inline Deque() noexcept = default;

        [[nodiscard]] ScriptObjectHandle<Any> GetValue(std::int32_t index) noexcept override;

        [[nodiscard]] bool SetValue(std::int32_t index, Any* value) noexcept override;

        void Prepend(Any* value) noexcept;

        void Append(Any* value) noexcept;

        [[nodiscard]] Any* PopBack() noexcept;

        [[nodiscard]] Any* PopFront() noexcept;

        [[nodiscard]] Any* Erase(int32_t index) noexcept override;

        [[nodiscard]] int32_t GetSize() const noexcept override;

        [[nodiscard]] bool Clear() noexcept override;

        [[nodiscard]] bool Contains(Any* value) const noexcept override;

        [[nodiscard]] int32_t Count(Any* value) const noexcept override;

        [[nodiscard]] int32_t IndexOf(Any* value) const noexcept override;

        [[nodiscard]] int32_t ReverseIndexOf(Any* value) const noexcept override;

        [[nodiscard]] ValueIterator* GetValueIterator() noexcept override;

        [[nodiscard]] ValueIterator* GetReverseValueIterator() noexcept override;

        [[nodiscard]] ValueIterator* GetValueIteratorFrom(int32_t index) noexcept override;

        [[nodiscard]] ValueIterator* GetReverseValueIteratorFrom(int32_t index) noexcept override;

        [[nodiscard]] inline uint64_t GetVersion() const noexcept {
            return _version;
        }

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_value, "value");
            ar <=> articuno::kv(_version, "version");
        }

        mutable std::recursive_mutex _lock;
        std::deque<ScriptObjectHandle<Any>> _value;
        uint64_t _version{0};

        friend class DequeIterator;
        friend class articuno::access;
    };

    class DequeIterator : public ValueIterator {
        ScriptType(DequeIterator);

    public:
        inline DequeIterator() noexcept = default;

        DequeIterator(ScriptObjectHandle<Deque> parent, bool reverse) noexcept;

        DequeIterator(ScriptObjectHandle<Deque> parent, bool reverse, int32_t index) noexcept;

        [[nodiscard]] bool IsValid() noexcept override;

        [[nodiscard]] bool HasMore() noexcept override;

        [[nodiscard]] Any* GetValue() noexcept override;

        bool SetValue(Any* value) noexcept override;

        [[nodiscard]] bool Next() noexcept override;

    private:
        articuno_serde(ar) {
            ar <=> articuno::kv(_parent, "parent");
            ar <=> articuno::kv(_index, "index");
            ar <=> articuno::kv(_version, "version");
            ar <=> articuno::kv(_reverse, "reverse");
            ar <=> articuno::kv(_value, "value");
        }

        ScriptObjectHandle<Deque> _parent;
        std::size_t _index{0};
        uint64_t _version{0};
        bool _reverse{false};
        ScriptObjectHandle<Any> _value;

        friend class articuno::access;
    };
}
