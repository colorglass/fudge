#pragma once

#include <FDGE/Config/FileProxy.h>

#include "Proxy.h"

namespace FDGE::Binding::Papyrus {
    class FileProxy : public Proxy {
    ScriptType(FileProxy)
    public:

    private:
        //FDGE::Config::FileProxy _proxy;
        std::filesystem::path _path;
    };
}
