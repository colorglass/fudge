#pragma once

#include <FDGE/Util/Maps.h>

#include "VersionedIdentifier.h"

namespace FDGE::Dymod {
    class LoadOrderRules {
    public:
        using id_version_map = Util::istr_flat_hash_map<std::u8string,
                std::pair<gluino::semantic_version, gluino::semantic_version>>;

        [[nodiscard]] inline id_version_map& GetBlacklist() noexcept {
            return _blacklist._map;
        }

        [[nodiscard]] inline const id_version_map& GetBlacklist() const noexcept {
            return _blacklist._map;
        }

        [[nodiscard]] inline id_version_map& GetOverrideEnable() noexcept {
            return _overrideEnable._map;
        }

        [[nodiscard]] inline const id_version_map& GetOverrideEnable() const noexcept {
            return _overrideEnable._map;
        }

    private:
        struct VersionedIdentifierSet {
            articuno_deserialize(ar) {
                for (auto& key : ar) {
                    auto versions = std::make_pair(
                            gluino::semantic_version(0, 0, 0),
                            gluino::semantic_version(std::numeric_limits<uint16_t>::max(),
                                                     std::numeric_limits<uint16_t>::max(),
                                                     std::numeric_limits<uint16_t>::max())
                    );
                    articuno::kv(versions.first, "minVersion");
                    articuno::kv(versions.second, "maxVersion");
                    std::u8string id;
                    articuno::kv(id, "id");
                    _map.try_emplace(id, std::move(versions));
                }
            }

            id_version_map _map;
        };

        articuno_serde(ar) {
            ar <=> articuno::kv(_blacklist, "blacklist");
            ar <=> articuno::kv(_overrideEnable, "overrideEnable");
        }

        VersionedIdentifierSet _blacklist;
        VersionedIdentifierSet _overrideEnable;

        friend class articuno::access;
    };
}
