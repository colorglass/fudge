#pragma once

#include <FDGE/Util/FilesystemWatcher.h>
#include <FDGE/Util/Maps.h>
#include <FDGE/Util/SharedMutex.h>

#include "DymodProxy.h"
#include "LoadOrderRules.h"
#include "Mod.h"

namespace FDGE::Dymod {
#pragma warning(push)
#pragma warning(disable: 4251)
    class __declspec(dllexport) DymodLoader {
    public:
        void Refresh();

        void Emit(const DynamicEvent& event);

        [[nodiscard]] inline const std::vector<std::shared_ptr<Mod>>& GetLoadOrder() const noexcept {
            return _loadOrder;
        }

        [[nodiscard]] inline LoadOrderRules& GetLoadOrderRules() noexcept {
            return _loadOrderRules;
        }

        [[nodiscard]] inline const LoadOrderRules& GetLoadOrderRules() const noexcept {
            return _loadOrderRules;
        }

        [[nodiscard]] static DymodLoader& GetSingleton() noexcept;

    private:
        inline DymodLoader() noexcept
                : _watcher("Data\\Dymods",
                           [this](const auto& path, auto eventType) { HandleFilesystemEvent(path, eventType); }, true) {
        }

        void SortLoadOrder();

        void ValidateDependencies();

        void HandleFilesystemEvent(const FDGE_WIN_STRING_TYPE& path, Util::FilesystemEvent eventType);

        Util::SharedMutex _lock;
        Util::istr_flat_hash_map<std::u8string, std::shared_ptr<Mod>> _mods;
        std::vector<std::shared_ptr<Mod>> _loadOrder;
        Util::FilesystemWatcher _watcher;
        LoadOrderRules _loadOrderRules;
    };
#pragma warning(pop)
}
