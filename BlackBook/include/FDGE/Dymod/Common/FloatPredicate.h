#pragma once

#include <parallel_hashmap/phmap.h>

#include "../Predicate.h"

namespace FDGE::Dymod::Common {
    class FloatPredicate : public Predicate {
    public:
        [[nodiscard]] inline std::vector<std::pair<float_t, float_t>>& GetRanges() noexcept {
            return _ranges;
        }

        [[nodiscard]] inline const std::vector<std::pair<float_t, float_t>>& GetRanges() const noexcept {
            return _ranges;
        }

        [[nodiscard]] inline bool IsInclusiveMinimum() const noexcept {
            return _inclusiveMinimum;
        }

        inline void SetInclusiveMinimum(bool inclusiveMinimum) noexcept {
            _inclusiveMinimum = inclusiveMinimum;
        }

        [[nodiscard]] inline bool IsInclusiveMaximum() const noexcept {
            return _inclusiveMaximum;
        }

        inline void SetInclusiveMaximum(bool inclusiveMaximum) noexcept {
            _inclusiveMaximum = inclusiveMaximum;
        }

    protected:
        virtual std::optional<float_t> GetValue(const gluino::polymorphic_any& target) const noexcept = 0;

        [[nodiscard]] PredicateResult TestImpl(const gluino::polymorphic_any& target) const noexcept;

    private:
        bool _inclusiveMinimum{true};
        bool _inclusiveMaximum{false};
        std::vector<std::pair<float_t, float_t>> _ranges;
    };
}
