#pragma once

#include <cstdint>

#include <gluino/enumeration.h>

namespace FDGE::Dymod::Common {
    gluino_enum(SnapshotStrategy, uint8_t,
        (Disable, 0),
        (Enable, 1),
        (Force, 2));
}
