#pragma once

#include <RE/Skyrim.h>

#include "StringPredicate.h"

namespace FDGE::Dymod::Common {
    class EditorIDPredicate : public StringPredicate {
    protected:
        [[nodiscard]] inline std::optional<std::string> GetValue(
                const gluino::polymorphic_any& target) const noexcept override;
    };
}
