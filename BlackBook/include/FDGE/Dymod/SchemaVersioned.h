#pragma once

#include <cstdint>

#include <articuno/articuno.h>
#include <gluino/semantic_version.h>

namespace FDGE::Dymod {
    class SchemaVersioned {
    public:
        [[nodiscard]] inline const gluino::semantic_version GetSchemaVersion() const noexcept {
            return _schemaVersion;
        }

        inline void SetSchemaVersion(gluino::semantic_version schemaVersion) noexcept {
            _schemaVersion = std::move(schemaVersion);
        }

    protected:
        inline SchemaVersioned() noexcept = default;

        articuno_serde(ar, flags) {
            ar <=> articuno::kv(_schemaVersion, "schemaVersion", flags);
        }

    private:
        gluino::semantic_version _schemaVersion{1};

        friend class articuno::access;
    };
}
