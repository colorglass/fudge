#pragma once

#include <memory>
#include <vector>

#include "Node.h"

namespace FDGE::Dymod {
    class Action;

    class Predicate;

    class Rule : public Node {
    public:
        [[nodiscard]] inline std::vector<std::unique_ptr<Predicate>>& GetPredicates() noexcept {
            return _predicates;
        }

        [[nodiscard]] inline const std::vector<std::unique_ptr<Predicate>>& GetPredicates() const noexcept {
            return _predicates;
        }

        [[nodiscard]] inline std::vector<std::unique_ptr<Action>>& GetActions() noexcept {
            return _actions;
        }

        [[nodiscard]] inline const std::vector<std::unique_ptr<Action>>& GetActions() const noexcept {
            return _actions;
        }

        [[nodiscard]] inline std::vector<Rule>& GetThenRules() noexcept {
            return _thenRules;
        }

        [[nodiscard]] inline const std::vector<Rule>& GetThenRules() const noexcept {
            return _thenRules;
        }

        [[nodiscard]] inline std::vector<Rule>& GetElseRules() noexcept {
            return _elseRules;
        }

        [[nodiscard]] inline const std::vector<Rule>& GetElseRules() const noexcept {
            return _elseRules;
        }

    private:
        std::vector<std::unique_ptr<Predicate>> _predicates;
        std::vector<std::unique_ptr<Action>> _actions;
        std::vector<Rule> _thenRules;
        std::vector<Rule> _elseRules;
    };
}
