#pragma once

#include <RE/Skyrim.h>

namespace FDGE::Skyrim {
    class CellsLoadingEvent {
    public:
        template <class T = RE::TESObjectREFR>
        class Reference {
        public:
            [[nodiscard]] inline gluino::safe_ptr<T> GetForm() const noexcept {
                return _form;
            }

            [[nodiscard]] inline bool IsInitializing() const noexcept {
                return _initializing;
            }

        private:
            gluino::safe_ptr<T> _form;
            bool _initializing;
        };

        class Cell {
        public:
            [[nodiscard]] inline std::span<Reference<RE::Actor>> GetActors() const noexcept {
                return std::span<Reference<RE::Actor>>(
                        reinterpret_cast<std::vector<Reference<RE::Actor>>*>(&_references)->begin(), _actorsSize);
            }

            [[nodiscard]] inline std::span<Reference<>> GetReferences() const noexcept {
                return std::span<Reference<>>(_references.begin(), _references.size());
            }

        private:
            mutable std::vector<Reference<>> _references;
            std::size_t _actorsSize;
        };

        [[nodiscard]] inline std::span<const Cell> GetCells() const noexcept {
            return std::span<const Cell>(_cells.begin(), _cells.size());
        }

    private:
        std::vector<Cell> _cells;
    };
}
