#pragma once

#include <gluino/ptr.h>
#include <RE/Skyrim.h>

namespace FDGE::Skyrim {
    class ActorEvent {
    public:
        [[nodiscard]] inline gluino::safe_ptr<RE::Actor> GetActor() const noexcept {
            return _actor;
        }

        [[nodiscard]] inline bool IsInitializing() const noexcept {
            return _initializing;
        }

    protected:
        inline ActorEvent(gluino::safe_ptr<RE::Actor> actor, bool initializing) noexcept
                : _actor(actor), _initializing(initializing) {
        }

    private:
        gluino::safe_ptr<RE::Actor> _actor;
        bool _initializing;
    };

    class ActorLoadedEvent : public ActorEvent {
    public:
        inline ActorLoadedEvent(gluino::safe_ptr<RE::Actor> actor, bool initializing) noexcept
                : ActorEvent(actor, initializing) {
        }
    };

    class ActorSpawnedEvent : public ActorEvent {
    public:
        inline ActorSpawnedEvent(gluino::safe_ptr<RE::Actor> actor, bool initializing) noexcept
                : ActorEvent(actor, initializing) {
        }
    };
}
