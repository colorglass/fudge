#pragma once

#include <gluino/preprocessor.h>
#include <RE/Skyrim.h>

#include "../../Plugin.h"

namespace FDGE::Binding::Papyrus::detail {
    class PapyrusClass {
    public:
        [[nodiscard]] static RE::BSTSmartPointer<RE::BSScript::Stack> GetStack(RE::VMStackID stackID) noexcept;

    protected:
        inline PapyrusClass() noexcept = default;

        class Function {
        public:
            inline explicit Function(std::string_view className, std::string_view functionName,
                                            RE::BSScript::IVirtualMachine const* vm) noexcept
                    : _className(className), _functionName(functionName), _vm(vm) {
            }

            template <class Return, class... Args>
            void operator>>(Return(*function)(const gluino::safe_ptr<RE::BSScript::Internal::VirtualMachine>,
                    const RE::VMStackID, Args...)) {
                const_cast<RE::BSScript::IVirtualMachine*>(_vm)->RegisterFunction(
                        _functionName, _className, reinterpret_cast<Return(*)(RE::BSScript::Internal::VirtualMachine*, RE::VMStackID, Args...)>(
                                function));
            }

        private:
            std::string_view _className;
            std::string_view _functionName;
            RE::BSScript::IVirtualMachine const* _vm;

            friend class PapyrusClass;
        };
    };
}

#define PapyrusClass(name) namespace { \
    struct GLUINO_CONCAT(PapyrusClass, name) : public ::FDGE::Binding::Papyrus::detail::PapyrusClass { \
        inline GLUINO_CONCAT(PapyrusClass, name)() {                                                   \
            ::SKSE::GetPapyrusInterface()->Register(+[](::RE::BSScript::Internal::VirtualMachine* vm) {         \
                Initialize(#name, vm); \
                return true;           \
            });                        \
        }                              \
                                       \
        private:                       \
            static inline void Initialize(std::string_view ClassName, ::RE::BSScript::Internal::VirtualMachine const* VirtualMachine); \
    };                                 \
    OnSKSELoaded {                     \
        GLUINO_CONCAT(PapyrusClass, name) GLUINO_CONCAT(__papyrusClass, name);                         \
    }                                  \
}                                      \
void GLUINO_CONCAT(PapyrusClass, name)::Initialize([[maybe_unused]] const std::string_view ClassName,  \
    [[maybe_unused]] ::RE::BSScript::Internal::VirtualMachine const* VirtualMachine)

#define PapyrusFunction(name, ...) Function(ClassName, #name, VirtualMachine) >> +[]([[maybe_unused]] const ::gluino::safe_ptr<::RE::BSScript::Internal::VirtualMachine> VirtualMachine, [[maybe_unused]] const ::RE::VMStackID StackID, __VA_ARGS__)

#define PapyrusStaticFunction(name, ...) Function(ClassName, #name, VirtualMachine) >> +[]([[maybe_unused]] const ::gluino::safe_ptr<::RE::BSScript::Internal::VirtualMachine> VirtualMachine, [[maybe_unused]] const ::RE::VMStackID StackID, ::RE::StaticFunctionTag* __VA_OPT__(,) __VA_ARGS__)

#define CurrentCallStack GetStack(StackID)
