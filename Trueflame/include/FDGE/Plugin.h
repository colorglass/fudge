
#pragma once

#include <array>
#include <limits>

#include <gluino/preprocessor.h>
#include <gluino/ptr.h>
#include <errhandlingapi.h>
#include <libloaderapi.h>
#include <SKSE/SKSE.h>

EXTERN_C IMAGE_DOS_HEADER __ImageBase;

namespace FDGE {
    namespace detail {
        template <std::size_t Index, char C>
        constexpr uint8_t ReadVersion(std::array<typename REL::Version::value_type, 4>& result) {
            static_assert(C >= '0' && C <= '9', "Invalid character in semantic version literal.");
            static_assert(Index < 4, "Too many components in semantic version literal.");
            result[Index] += (C - '0');
            return 1;
        }

        template <std::size_t Index, char C, char... Rest>
        requires (sizeof...(Rest) > 0)
        constexpr uint8_t ReadVersion(std::array<typename REL::Version::value_type, 4>& result) {
            static_assert(C == '.' || (C >= '0' && C <= '9'), "Invalid character in semantic version literal.");
            static_assert(Index < 4, "Too many components in semantic version literal.");
            if constexpr (C == '.') {
                ReadVersion<Index + 1, Rest...>(result);
                return 0;
            } else {
                auto position = ReadVersion<Index, Rest...>(result);
                result[Index] += (C - '0') * std::pow(10, position);
                return position + 1;
            }
        }

        class LoadInitializer {
        public:
            LoadInitializer(std::function<void(const SKSE::LoadInterface&)> callback, uint64_t priority) noexcept;
        };

        class MessageHandler {
        public:
            MessageHandler(std::function<void(const SKSE::MessagingInterface::Message&)> callback, uint32_t messageType,
                           uint64_t priority) noexcept;
        };

        class Plugin {
        public:
            template <std::size_t Length = 256>
            class CharacterBuffer {
            public:
                constexpr CharacterBuffer& operator=(std::string_view str) noexcept {
                    std::span<char> buffer(_buffer);
                    std::fill(buffer.begin(), buffer.end(), '\0');
                    std::copy(str.begin(), str.end(), buffer.begin());
                    return *this;
                }

                constexpr operator const char*() noexcept {
                    return _buffer;
                }

                constexpr operator bool() noexcept {
                    return _buffer[0] != '\0';
                }

            private:
                char _buffer[Length]{};
            };

            class Version {
            public:
                constexpr Version() noexcept = default;

                constexpr Version(REL::Version version) noexcept {
                    this->operator=(version);
                }

                constexpr Version& operator=(REL::Version version) noexcept {
                    _version = version.pack();
                    return *this;
                }

                constexpr Version& operator=(const REL::Version::value_type values[4]) noexcept {
                    return operator=(REL::Version(values[0], values[1], values[2], values[3]));
                }

                [[nodiscard]] constexpr explicit operator uint32_t() const noexcept {
                    return _version;
                }

            private:
                uint32_t _version{};
            };

            template <std::size_t Length = 16>
            class VersionList;

            class VersionIndependent {
            public:
                [[nodiscard]] constexpr static VersionIndependent ViaAddressLibrary() noexcept {
                    return VersionIndependent(true, false);
                }

                [[nodiscard]] constexpr static VersionIndependent ViaSignatureScanning() noexcept {
                    return VersionIndependent(false, true);
                }

                [[nodiscard]] constexpr static VersionIndependent ViaAddressLibraryAndSignatureScanning() noexcept {
                    return VersionIndependent(true, true);
                }

            private:
                constexpr VersionIndependent(bool addressLibrary, bool sigScanning) noexcept
                        : _addressLibrary(addressLibrary), _sigScanning(sigScanning) {
                }

                bool _addressLibrary;
                bool _sigScanning;

                template <std::size_t Length>
                friend class VersionList;
            };

            template <std::size_t Length>
            class VersionList {
            public:
                constexpr VersionList() noexcept {
                    _versions[0] = SKSE::RUNTIME_SSE_LATEST.pack();
                }

                constexpr VersionList& operator=(std::initializer_list<REL::Version> versions) noexcept {
                    std::transform(versions.begin(), versions.end(), std::begin(_versions),
                                   [](const REL::Version& ver) noexcept {
                                       return ver.pack();
                                   });
                    return *this;
                }

                constexpr VersionList& operator=(VersionIndependent versionIndependence) noexcept {
                    std::span<uint32_t> versions(_versions);
                    std::fill(versions.begin(), versions.end(), 0);
                    _addressLibrary = versionIndependence._addressLibrary;
                    _sigScanning = versionIndependence._sigScanning;
                    return *this;
                }

                constexpr VersionList& operator=(REL::Version version) noexcept {
                    this->operator=({version});
                    return *this;
                }

            private:
                bool _addressLibrary: 1 = true;
                bool _sigScanning: 1 = false;
                uint8_t _padding: 6 = 0;
                std::uint8_t _padding2 = 0;
                std::uint16_t _padding3 = 0;
                std::uint32_t _versions[Length]{};

                friend class Plugin;
            };

            class Record {
            public:
                constexpr Record() noexcept = default;

                constexpr Record(const char value[5]) noexcept {
                    this->operator=(value);
                }

                constexpr Record& operator=(const char value[5]) noexcept {
                    _value = value[0] + (value[1] << 8) + (value[2] << 16) + (value[3] << 24);
                    return *this;
                }

                constexpr operator uint32_t() noexcept {
                    return _value;
                }

            private:
                uint32_t _value{0};
            };

            template <std::size_t N>
            class StringList {
            public:
                constexpr StringList() noexcept = default;

                constexpr StringList& operator=(std::string_view string) noexcept {
                    return (*this) = {string};
                }

                constexpr StringList& operator=(std::initializer_list<std::string_view> strings) noexcept {
                    std::copy(strings.begin(), strings.end(), _strings.begin());
                    return *this;
                }

                constexpr auto begin() noexcept {
                    return _strings.begin();
                }

                constexpr auto end() noexcept {
                    return _strings.end();
                }

                constexpr operator bool() noexcept {
                    return !_strings[0].empty();
                }

                constexpr std::string_view operator[](std::size_t index) noexcept {
                    return _strings[index];
                }

            private:
                std::array<std::string_view, N> _strings;
            };

            [[nodiscard]] static gluino::optional_ptr<Plugin> Lookup() noexcept {
                auto* plugin = reinterpret_cast<Plugin*>(
                        GetProcAddress(reinterpret_cast<HMODULE>(&__ImageBase), "SKSEPlugin_Version"));
                auto* usingPlugin = reinterpret_cast<bool*>(
                        GetProcAddress(reinterpret_cast<HMODULE>(&__ImageBase), "__FDGEUsingPlugin"));
                if (plugin && usingPlugin) {
                    return plugin;
                }
                return {};
            }

        protected:
            constexpr Plugin() noexcept = default;

        private:
            std::uint32_t _dataVersion{1};

        public:
            /**
             * The version number of your plugin (defaults to version 1.0.0.0).
             */
            class Version Version{REL::Version(1)};

            /**
             * The name of your plugin.
             *
             * <p>
             * This is helpful for debugging and metadata, but also used by many Trueflame and FuDGE components. For
             * example it is the default file name for your log files, and the default module name that associates your
             * plugin with FuDGE co-saves.
             * </p>
             */
            CharacterBuffer<> Name;

            /**
             * The name of the plugin author.
             */
            CharacterBuffer<> Author;

            /**
             * An email address for support requests.
             */
            CharacterBuffer<> Email;

        public:
            /**
             * Defines the compatibility of your plugin with different versions of the Skyrim runtime.
             *
             * <p>
             * This can be either a list of versions which are supported, or you may set it to be version independent
             * via either Address Library (VersionIndependent::ViaAddressLibrary) or by signature scanning
             * (VersionIndependent::ViaSignatureScanning) or both
             * (VersionIndependent::ViaAddressLibraryAndSignatureScanning). The default value is version independent via
             * address library. When set to a specific list of Skyrim versions, the current version of Skyrim must match
             * one of them or SKSE will not load your plugin (with a visible warning to the user).
             * </p>
             */
            VersionList<> CompatibleSkyrimVersions;

            /**
             * The minimum SKSE version to support.
             *
             * <p>
             * This should generally not be modified, unless you really know what you are doing.
             * </p>
             */
            class Version SKSEMinimum;

        private:
            std::uint64_t _padding4[4]{}; // Extra padding for future-proofing.

        public:
            /**
             * A record type that will be associated with your plugin to use when integrating with the SKSE co-save.
             *
             * <p>
             * This is a 4-byte record (4 characters) that uniquely identifies your plugin. It must not conflict with
             * any other plugin and must remain the same so that previously saved data can be found during loading of a
             * save later. Trueflame will automatically register with SKSE using this record type during initialization.
             * </p>
             *
             * <p>
             * If coding for Fully Dynamic Game Engine, use of the FuDGE co-save is recommended over the SKSE co-save.
             * </p>
             */
            Record SKSESerializationPluginID;

            /**
             * The record which will be used by the Trueflame SKSESerializationHook to centralize all save data for
             * the SKSE co-save.
             *
             * <p>
             * The SKSESerializationHook saves all data under a single SKSE co-save record, which it multiplexes across
             * all hooks, using this record type. If using SKSE's serialization interface directly this value has no
             * effect.
             * </p>
             */
            Record SKSESerializationRecordID{"SAVE"};

            /**
             * A function to execute immediately upon the start of your plugin's initialization.
             *
             * <p>
             * This precedes even the declarative SKSE load handlers and other checks. It primarily exists to allow for
             * logging to be initialized before any other part of the Trueflame initialization occurs, so that you can
             * get log information from that process.
             * </p>
             */
            void(*PreInit)(){nullptr};

            /**
             * Whether this plugin should be loaded in the Creation Kit (defaults to false).
             */
            bool SupportsCreationKit{false};

            /**
             * Whether to use Trueflame declarative messaging (defaults to true).
             *
             * <p>
             * Declarative messaging lets you declare message handlers independently in different source code modules.
             * To handle this Trueflame must register a message listener. Since only one listener can be registered for
             * a given mod, this prevents you from registering with the SKSE messaging interface for your own listeners
             * (i.e. when used all message handling must be done via Trueflame's declarative listeners). If you want to
             * use the traditional messaging APIs then set this to false.
             * </p>
             */
            bool UsesDeclarativeMessaging{true};

            /**
             * A list of alternative names to use for the FuDGE co-save serialization hooks.
             *
             * <p>
             * When using FuDGE co-saves (not to be confused with traditional SKSE co-saves) a full module name is used
             * to identify the plugin which owns the save data, rather than the SKSE co-save's 4-byte record. By default
             * this will be the name of your plugin, as identified in your plugin declaration. However this value can
             * be overridden by providing one or more names here. The first name in this list will be used as the
             * module name when saving data. When loading data FuDGE will invoke the load hooks for all names in this
             * list, thus allowing backwards compatibility in the event of a preferred name change.
             * </p>
             *
             * <p>
             * This can be at most 16 names.
             * </p>
             */
            StringList<16> SerializationModuleNames;
        };
    };

    /**
     * Indicates that an SKSE plugin is not compatible with this environment and should not be loaded.
     *
     * <p>
     * In a FuDGE loading handler, which does not return a boolean to indicate success or failure as SKSE's handlers do,
     * throwing this exception is how you can indicate a plugin should not be loaded. If this is thrown the plugin will
     * be omitted. SKSE will log that it was not loaded but otherwise the user is not notified. Failures of other types,
     * such as other exceptions or Windows SEH errors, will trigger SKSE to show a message box warning the user of the
     * error and asking if they should like to stop Skyrim from starting.
     * </p>
     */
    class PluginIncompatible : public std::exception {
    public:
        using std::exception::exception;
    };
};

inline namespace Literals {
    inline namespace FDGELiterals {
        template <char... C>
        [[nodiscard]] constexpr REL::Version operator ""_v() {
            std::array<typename REL::Version::value_type, 4> result{0, 0, 0, 0};
            FDGE::detail::ReadVersion<0, C...>(result);
            return REL::Version(result);
        }
    }
}

/**
 * Declare your SKSE plugin.
 *
 * <p>
 * This declaration makes it possible for your plugin to be identified and loaded by SKSE. The macro takes a code block
 * argument, in which various properties can be set on <code>this</code>. See the documentation for each property for
 * more information.
 * </p>
 */
#define SKSEPlugin(...) struct PluginDeclaration : public ::FDGE::detail::Plugin { \
    constexpr PluginDeclaration() noexcept { __VA_ARGS__ }                         \
};                                                                                 \
EXTERN_C __declspec(dllexport) constinit bool __FDGEUsingPlugin{true};             \
EXTERN_C __declspec(dllexport) constinit auto SKSEPlugin_Version = PluginDeclaration()

#define __OnSKSELoad(priority, counter) inline void GLUINO_CONCAT(__OnSKSELoadCallback, counter) (const ::SKSE::LoadInterface& LoadInterface); \
::FDGE::detail::LoadInitializer GLUINO_CONCAT(__OnSKSELoad, counter) (GLUINO_CONCAT(__OnSKSELoadCallback, counter), priority);                 \
inline void GLUINO_CONCAT(__OnSKSELoadCallback, counter) ([[maybe_unused]] const ::SKSE::LoadInterface& LoadInterface)

/**
 * Declares a function to be invoked when SKSE is loading this SKSE plugin.
 *
 * @param priority The priority of the load handler; handlers with lower values run before those with higher values.
 */
#define OnSKSELoad(priority) __OnSKSELoad(priority, __COUNTER__)

/**
 * Declares a function to be invoked when SKSE begins loading this SKSE plugin.
 *
 * <p>
 * This call is equivalent to <code>OnSKSELoad(0)</code>, and therefore shorthand for a handler which runs before all
 * other handlers. It's order against other handlers of priority zero is undefined.
 * </p>
 */
#define OnSKSELoading OnSKSELoad(0)

/**
 * Declares a function to be invoked when SKSE finishes loading this SKSE plugin.
 *
 * <p>
 * This call is equivalent to <code>OnSKSELoad(UINT64_MAX)</code>, and therefore shorthand for a handler which runs
 * after all others. It's order against other handlers of priority <code>UINT64_MAX</code> is undefined.
 */
#define OnSKSELoaded OnSKSELoad((::std::numeric_limits<uint64_t>::max)())

#define __OnSKSEMessage(messageType, priority, counter, ...) inline void GLUINO_CONCAT(__OnSKSEMessageCallback, counter) (const ::SKSE::MessagingInterface::Message& __VA_ARGS__); \
::FDGE::detail::MessageHandler GLUINO_CONCAT(__OnSKSEMessage, counter) (GLUINO_CONCAT(__OnSKSEMessageCallback, counter), static_cast<uint32_t>(messageType), priority);   \
inline void GLUINO_CONCAT(__OnSKSEMessageCallback, counter) (const ::SKSE::MessagingInterface::Message& __VA_ARGS__)

/**
 * Declares a handler for a function to be invoked on receiving an SKSE message.
 *
 * @param messageType The type of the message to handle.
 * @param priority The priority of the message handler; handlers with lower priority are run before others.
 * @param ... An optional name for the variable that will assigned the message.
 */
#define OnSKSEMessage(messageType, priority, ...) __OnSKSEMessage(messageType, priority, __COUNTER__, __VA_ARGS__)

/**
 * Declares a handler for a function to be invoked on receiving an SKSE message.
 *
 * <p>
 * This is shorthand for <code>OnSKSEMessage(0)</code>, and therefore runs before all other handlers. It's order with
 * respect to other priority 0 handlers is undefined.
 * </p>
 *
 * @param messageType The type of the message to handle.
 * @param ... An optional name for the variable that will assigned the message.
 */
#define OnSKSEReceivingMessage(messageType, ...) OnSKSEMessage(messageType, 0, __VA_ARGS__)

/**
 * Declares a handler for a function to be invoked after receiving an SKSE message.
 *
 * <p>
 * This is shorthand for <code>OnSKSEMessage(UINT64_MAX)</code>, and therefore runs before all other handlers. It's
 * order with respect to other priority 0 handlers is undefined.
 * </p>
 *
 * @param messageType The type of the message to handle.
 * @param ... An optional name for the variable that will assigned the message.
 */
#define OnSKSEMessageReceived(messageType, ...) OnSKSEMessage(messageType, (::std::numeric_limits<uint64_t>::max)(), __VA_ARGS__)

/**
 * Declares an action to be taken after all plugins are loaded.
 *
 * <p>
 * This is shorthand for <code>OnSKSEReceivingMessage(kPostLoad, UINT64_MAX)</code>, and provided for convenience as
 * this is the point when all SKSE plugins are loaded and plugins can freely perform operations that might be blocked
 * during plugin loading.
 * </p>
 */
#define AfterSKSEPluginsLoaded OnSKSEMessageReceived(::SKSE::MessagingInterface::kPostLoad)
