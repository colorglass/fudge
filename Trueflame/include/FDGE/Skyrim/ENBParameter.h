#pragma once

#include <array>
#include <optional>

#include <gluino/enumeration.h>

namespace FDGE::Skyrim {
    // TODO: Implement Hex, Color, and Vector types.
    gluino_enum(ENBParameterType, long,
                (None, 0),
                (Float, 1),
                (Integer, 2),
                (Hexadecimal, 3),
                (Boolean, 4),
                (TriColor, 5),
                (QuadColor, 6),
                (TriVector, 7));

    class ENBParameter {
    public:
        using size_type = unsigned long;

        inline ENBParameter() noexcept = default;

        inline explicit ENBParameter(int32_t value) noexcept {
            SetInteger(value);
        }

        inline explicit ENBParameter(float_t value) noexcept {
            SetFloat(value);
        }

        inline explicit ENBParameter(bool value) noexcept {
            SetBoolean(value);
        }

        [[nodiscard]] inline const std::array<std::byte, 16>& GetRawData() const noexcept {
            return _data;
        }

        [[nodiscard]] inline size_type GetSize() const noexcept {
            return _size;
        }

        [[nodiscard]] inline ENBParameterType GetType() const noexcept {
            return _type;
        }

        [[nodiscard]] inline std::optional<int32_t> GetInteger() const noexcept {
            if (_type == ENBParameterType::Integer) {
                return *reinterpret_cast<const int32_t*>(_data.data());
            }
            return {};
        }

        inline void Reset() noexcept {
            Set(ENBParameterType::None, 0);
        }

        inline void SetInteger(bool value) noexcept {
            Set(ENBParameterType::Integer, 4);
            *reinterpret_cast<int32_t*>(_data.data()) = value;
        }

        [[nodiscard]] inline std::optional<float_t> GetFloat() const noexcept {
            if (_type == ENBParameterType::Float) {
                return *reinterpret_cast<const float_t*>(_data.data());
            }
            return {};
        }

        inline void SetFloat(float_t value) noexcept {
            Set(ENBParameterType::Float, 4);
            *reinterpret_cast<float_t*>(_data.data()) = value;
        }

        [[nodiscard]] inline std::optional<bool> GetBoolean() const noexcept {
            if (_type == ENBParameterType::Boolean) {
                return *reinterpret_cast<const bool*>(_data.data());
            }
            return {};
        }

        inline void SetBoolean(bool value) noexcept {
            Set(ENBParameterType::Boolean, 4);
            *reinterpret_cast<int32_t*>(_data.data()) = value;
        }

    private:
        inline void Set(ENBParameterType type, size_type size) {
            memset(_data.data(), 0, sizeof(_data));
            _type = type;
            _size = size;
        }

        std::array<std::byte, 16> _data;
        size_type _size;
        ENBParameterType _type;
    };
}
