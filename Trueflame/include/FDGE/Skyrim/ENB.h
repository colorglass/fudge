#pragma once

#include <gluino/ptr.h>

#include "ENBEvent.h"
#include "ENBParameter.h"
#include "ENBRenderInfo.h"
#include "../Util/EventSource.h"

namespace FDGE::Skyrim {
    class ENB : public Util::EventSource<ENBEvent> {
    public:
        using version_type = long;
        using position_type = long;

        virtual ~ENB() noexcept = default;

        [[nodiscard]] inline bool IsPresent() const noexcept {
            return GetSDKVersion().has_value();
        }

        [[nodiscard]] virtual std::optional<version_type> GetSDKVersion() const noexcept = 0;

        [[nodiscard]] virtual std::optional<version_type> GetVersion() const noexcept = 0;

        [[nodiscard]] virtual bool IsEnabled() const noexcept = 0;

        virtual bool SetEnabled(bool enabled) = 0;

        [[nodiscard]] virtual gluino::optional_ptr<ENBRenderInfo> GetRenderInfo() const noexcept = 0;

        [[nodiscard]] virtual bool IsEditorActive() const noexcept = 0;

        [[nodiscard]] virtual bool IsEffectsWindowActive() const noexcept = 0;

        [[nodiscard]] virtual position_type GetCursorXPosition() const noexcept = 0;

        [[nodiscard]] virtual position_type GetCursorYPosition() const noexcept = 0;

        [[nodiscard]] virtual std::pair<position_type, position_type> GetCursorPosition() const noexcept = 0;

        [[nodiscard]] virtual bool IsLeftMouseButtonPressed() const noexcept = 0;

        [[nodiscard]] virtual bool IsMiddleMouseButtonPressed() const noexcept = 0;

        [[nodiscard]] virtual bool IsRightMouseButtonPressed() const noexcept = 0;

        [[nodiscard]] virtual bool ReadParameter(std::string_view fileName, std::string_view category,
                                                 std::string_view key, ENBParameter& out) const noexcept = 0;

        [[nodiscard]] virtual bool WriteParameter(std::string_view fileName, std::string_view category,
                                                  std::string_view key, const ENBParameter& value) noexcept = 0;

        [[nodiscard]] bool WriteParameter(std::string_view fileName, std::string_view category, std::string_view key,
                                          const ENBParameter&& value) noexcept {
            return WriteParameter(fileName, category, key, value);
        }

        // TODO: Add option to save ENB settings.

        [[nodiscard]] static ENB& GetSingleton() noexcept;

    protected:
        inline ENB() noexcept = default;
    };
}
