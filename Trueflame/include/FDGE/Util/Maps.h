#pragma once

#include <metrohash.h>
#include <gluino/map.h>
#include <parallel_hashmap/phmap.h>

namespace FDGE::Util {
    template <class K, class Compare = gluino::str_less<true>, class Alloc = std::allocator<K>>
    using str_btree_set = phmap::btree_set<K, Compare, Alloc>;

    template <class K, class Compare = gluino::str_less<false>, class Alloc = std::allocator<K>>
    using istr_btree_set = phmap::btree_set<K, Compare, Alloc>;

    template <class K, class V, class Compare = gluino::str_less<true>, class Alloc = std::allocator<K>>
    using str_btree_map = phmap::btree_map<K, V, Compare, Alloc>;

    template <class K, class V, class Compare = gluino::str_less<false>, class Alloc = std::allocator<K>>
    using istr_btree_map = phmap::btree_map<K, V, Compare, Alloc>;

    template <class K, class Alloc = std::allocator<K>>
    using str_flat_hash_set =
    phmap::flat_hash_set<K, gluino::str_hash<true>, gluino::str_equal_to<true>, Alloc>;

    template <class K, class Alloc = std::allocator<K>>
    using istr_flat_hash_set =
    phmap::flat_hash_set<K, gluino::str_hash<false>, gluino::str_equal_to<false>, Alloc>;

    template <class K, class V, class Alloc = std::allocator<std::pair<K, V>>>
    using str_flat_hash_map =
    phmap::flat_hash_map<K, V, gluino::str_hash<true>, gluino::str_equal_to<true>, Alloc>;

    template <class K, class V, class Alloc = std::allocator<std::pair<K, V>>>
    using istr_flat_hash_map =
    phmap::flat_hash_map<K, V, gluino::str_hash<false>, gluino::str_equal_to<false>, Alloc>;

    template <class K, std::size_t N = 4, class Mutex = std::mutex, class Hash = std::hash<K>,
            class Eq = std::equal_to<K>, class Alloc = std::allocator<K>>
    using parallel_flat_hash_set =
    phmap::parallel_flat_hash_set<K, Hash, Eq, Alloc, N, Mutex>;

    template <class K, std::size_t N = 4, class Mutex = std::mutex, class Alloc = std::allocator<K>>
    using str_parallel_flat_hash_set =
    phmap::parallel_flat_hash_set<K, gluino::str_hash<true>, gluino::str_equal_to<true>, Alloc, N, Mutex>;

    template <class K, std::size_t N = 4, class Mutex = std::mutex, class Alloc = std::allocator<K>>
    using istr_parallel_flat_hash_set =
    phmap::parallel_flat_hash_set<K, gluino::str_hash<false>, gluino::str_equal_to<false>, Alloc, N, Mutex>;

    template <class K, class V, std::size_t N = 4, class Mutex = std::mutex, class Hash = std::hash<K>,
            class Eq = std::equal_to<K>, class Alloc = std::allocator<std::pair<K, V>>>
    using parallel_flat_hash_map =
    phmap::parallel_flat_hash_map<K, V, Hash, Eq, Alloc, N, Mutex>;

    template <class K, class V, std::size_t N = 4, class Mutex = std::mutex,
            class Alloc = std::allocator<std::pair<K, V>>>
    using str_parallel_flat_hash_map =
    phmap::parallel_flat_hash_map<K, V, gluino::str_hash<true>, gluino::str_equal_to<true>, Alloc, N, Mutex>;

    template <class K, class V, std::size_t N = 4, class Mutex = std::mutex,
            class Alloc = std::allocator<std::pair<K, V>>>
    using istr_parallel_flat_hash_map =
    phmap::parallel_flat_hash_map<K, V, gluino::str_hash<false>, gluino::str_equal_to<false>, Alloc, N, Mutex>;
}
