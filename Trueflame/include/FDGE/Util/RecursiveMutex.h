#pragma once

#include <mutex>

#pragma warning(push)
#pragma warning(disable: 4251)
/**
 * A recursive mutex class made for use across DLL boundaries, equivalent to <code>std::recursive_mutex</code>.
 */
namespace FDGE::Util {
    class __declspec(dllexport) RecursiveMutex {
    public:
        RecursiveMutex() noexcept;

        ~RecursiveMutex() noexcept;

        void lock() const noexcept;

        bool try_lock() const noexcept;

        void unlock() const noexcept;

    private:
        mutable std::recursive_mutex _lock;
    };
}
#pragma warning(pop)
