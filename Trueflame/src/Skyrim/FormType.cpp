#include <FDGE/Skyrim/FormType.h>

#include <FDGE/Util/Maps.h>

using namespace FDGE;
using namespace FDGE::Skyrim;
using namespace FDGE::Util;
using namespace phmap;

namespace {
    flat_hash_map<RE::FormType, std::pair<std::string_view, std::string_view>> FormTypesByType;
    istr_flat_hash_map<std::string_view, std::pair<RE::FormType, std::string_view>> FormTypesByRecordName;
    istr_flat_hash_map<std::string_view, std::pair<RE::FormType, std::string_view>> FormTypesByFriendlyName;

    void RegisterFormType(RE::FormType formType, std::string_view recordName, std::string_view friendlyName) {
        auto result = FormTypesByType.try_emplace(formType, recordName, friendlyName);
        FormTypesByRecordName.try_emplace(recordName, formType, friendlyName);
        FormTypesByFriendlyName.try_emplace(friendlyName, formType, recordName);
    }

    gluino_autorun {
        RegisterFormType(RE::FormType::PluginInfo, "TES4", "PluginInfo");
        RegisterFormType(RE::FormType::FormGroup, "GRUP", "FormGroup");
        RegisterFormType(RE::FormType::GameSetting, "GMST", "GameSetting");
        RegisterFormType(RE::FormType::Keyword, "KYWD", "Keyword");
        RegisterFormType(RE::FormType::LocationRefType, "LCRT", "LocationRefType");
        RegisterFormType(RE::FormType::Action, "AACT", "Action");
        RegisterFormType(RE::FormType::TextureSet, "TXST", "TextureSet");
        RegisterFormType(RE::FormType::MenuIcon, "MICN", "MenuIcon");
        RegisterFormType(RE::FormType::Global, "GLOB", "Global");
        RegisterFormType(RE::FormType::Class, "CLAS", "Class");
        RegisterFormType(RE::FormType::Faction, "FACT", "Faction");
        RegisterFormType(RE::FormType::HeadPart, "HDPT", "HeadPart");
        RegisterFormType(RE::FormType::Eyes, "EYES", "Eyes");
        RegisterFormType(RE::FormType::Race, "RACE", "Race");
        RegisterFormType(RE::FormType::Sound, "SOUN", "Sound");
        RegisterFormType(RE::FormType::AcousticSpace, "ASPC", "AcousticSpace");
        RegisterFormType(RE::FormType::Skill, "SKIL", "Skill");
        RegisterFormType(RE::FormType::MagicEffect, "MGEF", "MagicEffect");
        RegisterFormType(RE::FormType::Script, "SCPT", "Script");
        RegisterFormType(RE::FormType::LandTexture, "LTEX", "LandTexture");
        RegisterFormType(RE::FormType::Enchantment, "ENCH", "Enchantment");
        RegisterFormType(RE::FormType::Spell, "SPEL", "Spell");
        RegisterFormType(RE::FormType::Scroll, "SCRL", "Scroll");
        RegisterFormType(RE::FormType::Activator, "ACTI", "Activator");
        RegisterFormType(RE::FormType::TalkingActivator, "TACT", "TalkingActivator");
        RegisterFormType(RE::FormType::Armor, "ARMO", "Armor");
        RegisterFormType(RE::FormType::Book, "BOOK", "Book");
        RegisterFormType(RE::FormType::Container, "CONT", "Container");
        RegisterFormType(RE::FormType::Door, "DOOR", "Door");
        RegisterFormType(RE::FormType::Ingredient, "INGR", "Ingredient");
        RegisterFormType(RE::FormType::Light, "LIGH", "Light");
        RegisterFormType(RE::FormType::Misc, "MISC", "Misc");
        RegisterFormType(RE::FormType::Apparatus, "APPA", "Apparatus");
        RegisterFormType(RE::FormType::Static, "STAT", "Static");
        RegisterFormType(RE::FormType::StaticCollection, "SCOL", "StaticCollection");
        RegisterFormType(RE::FormType::MovableStatic, "MSTT", "MovableStatic");
        RegisterFormType(RE::FormType::Grass, "GRAS", "Grass");
        RegisterFormType(RE::FormType::Tree, "TREE", "Tree");
        RegisterFormType(RE::FormType::Flora, "FLOR", "Flora");
        RegisterFormType(RE::FormType::Furniture, "FURN", "Furniture");
        RegisterFormType(RE::FormType::Weapon, "WEAP", "Weapon");
        RegisterFormType(RE::FormType::Ammo, "AMMO", "Ammo");
        RegisterFormType(RE::FormType::NPC, "NPC_", "NPC");
        RegisterFormType(RE::FormType::LeveledNPC, "LVLN", "LeveledNPC");
        RegisterFormType(RE::FormType::KeyMaster, "KEYM", "KeyMaster");
        RegisterFormType(RE::FormType::AlchemyItem, "ALCH", "AlchemyItem");
        RegisterFormType(RE::FormType::IdleMarker, "IDLM", "IdleMarker");
        RegisterFormType(RE::FormType::Note, "NOTE", "Note");
        RegisterFormType(RE::FormType::ConstructibleObject, "COBJ", "ConstructibleObject");
        RegisterFormType(RE::FormType::Projectile, "PROJ", "Projectile");
        RegisterFormType(RE::FormType::Hazard, "HAZD", "Hazard");
        RegisterFormType(RE::FormType::SoulGem, "SLGM", "SoulGem");
        RegisterFormType(RE::FormType::LeveledItem, "LVLI", "LeveledItem");
        RegisterFormType(RE::FormType::Weather, "WTHR", "Weather");
        RegisterFormType(RE::FormType::Climate, "CLMT", "Climate");
        RegisterFormType(RE::FormType::ShaderParticleGeometryData, "SPGD", "ShaderParticleGeometryData");
        RegisterFormType(RE::FormType::ReferenceEffect, "RFCT", "ReferenceEffect");
        RegisterFormType(RE::FormType::Region, "REGN", "Region");
        RegisterFormType(RE::FormType::Navigation, "NAVI", "Navigation");
        RegisterFormType(RE::FormType::Cell, "CELL", "Cell");
        RegisterFormType(RE::FormType::Reference, "REFR", "Reference");
        RegisterFormType(RE::FormType::ActorCharacter, "ACHR", "ActorCharacter");
        RegisterFormType(RE::FormType::ProjectileMissile, "PMIS", "ProjectileMissile");
        RegisterFormType(RE::FormType::ProjectileArrow, "PARW", "ProjectileArrow");
        RegisterFormType(RE::FormType::ProjectileGrenade, "PGRE", "ProjectileGrenade");
        RegisterFormType(RE::FormType::ProjectileBeam, "PBEA", "ProjectileBeam");
        RegisterFormType(RE::FormType::ProjectileFlame, "PFLA", "ProjectileFlame");
        RegisterFormType(RE::FormType::ProjectileCone, "PCON", "ProjectileCone");
        RegisterFormType(RE::FormType::ProjectileBarrier, "PBAR", "ProjectileBarrier");
        RegisterFormType(RE::FormType::PlacedHazard, "PHZD", "PlacedHazard");
        RegisterFormType(RE::FormType::WorldSpace, "WRLD", "WorldSpace");
        RegisterFormType(RE::FormType::Land, "LAND", "Land");
        RegisterFormType(RE::FormType::NavMesh, "NAVM", "NavMesh");
        RegisterFormType(RE::FormType::TLOD, "TLOD", "TLOD");
        RegisterFormType(RE::FormType::Dialogue, "DIAL", "Dialogue");
        RegisterFormType(RE::FormType::Info, "INFO", "Info");
        RegisterFormType(RE::FormType::Quest, "QUST", "Quest");
        RegisterFormType(RE::FormType::Idle, "IDLE", "Idle");
        RegisterFormType(RE::FormType::Package, "PACK", "Package");
        RegisterFormType(RE::FormType::CombatStyle, "CSTY", "CombatStyle");
        RegisterFormType(RE::FormType::LoadScreen, "LSCR", "LoadScreen");
        RegisterFormType(RE::FormType::LeveledSpell, "LVSP", "LeveledSpell");
        RegisterFormType(RE::FormType::AnimatedObject, "ANIO", "AnimatedObject");
        RegisterFormType(RE::FormType::Water, "WATR", "Water");
        RegisterFormType(RE::FormType::EffectShader, "EFSH", "EffectShader");
        RegisterFormType(RE::FormType::TOFT, "TOFT", "TOFT");
        RegisterFormType(RE::FormType::Explosion, "EXPL", "Explosion");
        RegisterFormType(RE::FormType::Debris, "DEBR", "Debris");
        RegisterFormType(RE::FormType::ImageSpace, "IMGS", "ImageSpace");
        RegisterFormType(RE::FormType::ImageAdapter, "IMAD", "ImageAdapter");
        RegisterFormType(RE::FormType::FormList, "FLST", "FormList");
        RegisterFormType(RE::FormType::Perk, "PERK", "Perk");
        RegisterFormType(RE::FormType::BodyPartData, "BPTD", "BodyPartData");
        RegisterFormType(RE::FormType::AddonNode, "ADDN", "AddonNode");
        RegisterFormType(RE::FormType::ActorValueInfo, "AVIF", "ActorValueInfo");
        RegisterFormType(RE::FormType::CameraShot, "CAMS", "CameraShot");
        RegisterFormType(RE::FormType::CameraPath, "CPTH", "CameraPath");
        RegisterFormType(RE::FormType::VoiceType, "VTYP", "VoiceType");
        RegisterFormType(RE::FormType::MaterialType, "MATT", "MaterialType");
        RegisterFormType(RE::FormType::Impact, "IPCT", "Impact");
        RegisterFormType(RE::FormType::ImpactDataSet, "IPDS", "ImpactDataSet");
        RegisterFormType(RE::FormType::Armature, "ARMA", "Armature");
        RegisterFormType(RE::FormType::EncounterZone, "ECZN", "EncounterZone");
        RegisterFormType(RE::FormType::Location, "LCTN", "Location");
        RegisterFormType(RE::FormType::Message, "MESG", "Message");
        RegisterFormType(RE::FormType::Ragdoll, "RGDL", "Ragdoll");
        RegisterFormType(RE::FormType::DefaultObject, "DOBJ", "DefaultObject");
        RegisterFormType(RE::FormType::LightingMaster, "LGTM", "LightingMaster");
        RegisterFormType(RE::FormType::MusicType, "MUSC", "MusicType");
        RegisterFormType(RE::FormType::Footstep, "FSTP", "Footstep");
        RegisterFormType(RE::FormType::FootstepSet, "FSTS", "FootstepSet");
        RegisterFormType(RE::FormType::StoryManagerBranchNode, "SMBN", "StoryManagerBranchNode");
        RegisterFormType(RE::FormType::StoryManagerQuestNode, "SMQN", "StoryManagerQuestNode");
        RegisterFormType(RE::FormType::StoryManagerEventNode, "SMEN", "StoryManagerEventNode");
        RegisterFormType(RE::FormType::DialogueBranch, "DLBR", "DialogueBranch");
        RegisterFormType(RE::FormType::MusicTrack, "MUST", "MusicTrack");
        RegisterFormType(RE::FormType::DialogueView, "DLVW", "DialogueView");
        RegisterFormType(RE::FormType::WordOfPower, "WOOP", "WordOfPower");
        RegisterFormType(RE::FormType::Shout, "SHOU", "Shout");
        RegisterFormType(RE::FormType::EquipSlot, "EQUP", "EquipSlot");
        RegisterFormType(RE::FormType::Relationship, "RELA", "Relationship");
        RegisterFormType(RE::FormType::Scene, "SCEN", "Scene");
        RegisterFormType(RE::FormType::AssociationType, "ASTP", "AssociationType");
        RegisterFormType(RE::FormType::Outfit, "OTFT", "Outfit");
        RegisterFormType(RE::FormType::ArtObject, "ARTO", "ArtObject");
        RegisterFormType(RE::FormType::MaterialObject, "MATO", "MaterialObject");
        RegisterFormType(RE::FormType::MovementType, "MOVT", "MovementType");
        RegisterFormType(RE::FormType::SoundRecord, "SNDR", "SoundRecord");
        RegisterFormType(RE::FormType::DualCastData, "DUAL", "DualCastData");
        RegisterFormType(RE::FormType::SoundCategory, "SNCT", "SoundCategory");
        RegisterFormType(RE::FormType::SoundOutputModel, "SOPM", "SoundOutputModel");
        RegisterFormType(RE::FormType::CollisionLayer, "COLL", "CollisionLayer");
        RegisterFormType(RE::FormType::ColorForm, "CLFM", "ColorForm");
        RegisterFormType(RE::FormType::ReverbParam, "REVB", "ReverbParam");
        RegisterFormType(RE::FormType::LensFlare, "LENS", "LensFlare");
        RegisterFormType(RE::FormType::LensSprite, "LSPR", "LensSprite");
        RegisterFormType(RE::FormType::VolumetricLighting, "VOLI", "VolumetricLighting");
    };
}

FDGE::Skyrim::FormType::FormType(std::string_view name) {
    auto result = FormTypesByFriendlyName.find(name);
    if (result != FormTypesByFriendlyName.end()) {
        _value = result->second.first;
        return;
    }
    result = FormTypesByRecordName.find(name);
    if (result != FormTypesByRecordName.end()) {
        _value = result->second.first;
        return;
    }
    throw std::invalid_argument("String does not match a form type's friendly or record name.");
}

std::string_view FDGE::Skyrim::FormType::GetFriendlyName() const noexcept {
    auto result = FormTypesByType.find(_value);
    if (result == FormTypesByType.end()) {
        return "";
    }
    return result->second.second;
}

std::string_view FDGE::Skyrim::FormType::GetRecordName() const noexcept {
    auto result = FormTypesByType.find(_value);
    if (result == FormTypesByType.end()) {
        return "";
    }
    return result->second.first;
}
