#include <FDGE/Binding/Papyrus/PapyrusClass.h>

using namespace FDGE::Binding;
using namespace RE;

RE::BSTSmartPointer<RE::BSScript::Stack> Papyrus::detail::PapyrusClass::GetStack(RE::VMStackID stackID) noexcept {
    auto* vm = RE::BSScript::Internal::VirtualMachine::GetSingleton();
    RE::BSSpinLockGuard lock(vm->runningStacksLock);
    auto result = vm->allRunningStacks.find(stackID);
    if (result == vm->allRunningStacks.end()) {
        return {};
    }
    return result->second;
}
