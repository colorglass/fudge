#include <FDGE/Util/FilesystemWatcher.h>

#include <FDGE/Logger.h>
#include <gluino/autorun.h>

using namespace efsw;
using namespace FDGE;
using namespace FDGE::Util;

#ifdef FDGE_GENERIC_FILESYSTEM_WATCHER
#define __FDGE_GENERIC_FILESYSTEM_WATCHER true
#else
#define __FDGE_GENERIC_FILESYSTEM_WATCHER false
#endif

namespace {
    std::atomic_bool initialized;
}

FilesystemWatcher::FilesystemWatcher(const std::filesystem::path& path, callback_type callback, bool recursive)
        : _listener(std::move(callback)) {
    Logger::Trace("Creating filesystem watcher for path '{}'.", std::filesystem::absolute(path).string());
    _watcher = std::make_unique<efsw::FileWatcher>(__FDGE_GENERIC_FILESYSTEM_WATCHER);
    _watcher->addWatch(std::filesystem::absolute(path).string(), &_listener, recursive);
    _watcher->watch();
}

void FilesystemWatcher::Listener::handleFileAction(WatchID, const std::string& dir,
                                                   const std::string& filename, Action action,
                                                   std::string oldFilename) {
    auto event = static_cast<FilesystemEvent>(static_cast<std::underlying_type_t<Action>>(action) - 1);
    std::filesystem::path path = dir;
    path /= filename;
    Logger::Trace("Received filesystem event {} for path '{}'.", event.name(), path.string());
    _callback(path, event);
}
