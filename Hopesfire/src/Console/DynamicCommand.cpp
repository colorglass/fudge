#include <FDGE/Console/DynamicCommand.h>

using namespace FDGE::Console;

std::string_view DynamicCommand::GetNamespace() const noexcept {
    return _ns;
}

std::span<const std::string> DynamicCommand::GetNames() const noexcept {
    return std::span<const std::string>(_names.data(), _names.size());
}
