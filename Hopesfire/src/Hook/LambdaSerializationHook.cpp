#include <FDGE/Hook/LambdaSerializationHook.h>

using namespace FDGE;
using namespace FDGE::Hook;

void LambdaSerializationHook::OnGameSaving(std::ostream& out) {
    if (_stage == HandlerStage::Pre) {
        _saver(out);
    }
}

void LambdaSerializationHook::OnGameSaved(std::ostream& out) {
    if (_stage == HandlerStage::Post) {
        _saver(out);
    }
}

void LambdaSerializationHook::OnGameLoading(std::istream& in) {
    if (_stage == HandlerStage::Pre) {
        _loader(in);
    }
}

void LambdaSerializationHook::OnGameLoaded(std::istream& in) {
    if (_stage == HandlerStage::Post) {
        _loader(in);
    }
}
