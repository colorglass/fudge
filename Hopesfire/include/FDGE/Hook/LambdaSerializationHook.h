#pragma once

#include <functional>

#include "SerializationHook.h"

namespace FDGE::Hook {
    class LambdaSerializationHook : public SerializationHook {
    public:
        inline LambdaSerializationHook(std::string_view name, std::function<void(std::ostream&)>&& saver,
                                std::function<void(std::istream&)>&& loader,
                                HandlerStage stage = HandlerStage::Post)
                                : SerializationHook(name), _saver(saver), _loader(loader), _stage(stage) {
        }

    protected:
        void OnGameSaving(std::ostream& out) override;

        void OnGameSaved(std::ostream& out) override;

        void OnGameLoading(std::istream& in) override;

        void OnGameLoaded(std::istream& in) override;

    private:
        HandlerStage _stage;
        std::function<void(std::ostream&)> _saver;
        std::function<void(std::istream&)> _loader;
    };
}
