#pragma once

#include <RE/Skyrim.h>

namespace FDGE::Console {
    class __declspec(dllexport) Command {
    public:
        [[nodiscard]] virtual std::string_view GetNamespace() const noexcept = 0;

        [[nodiscard]] virtual std::span<const std::string> GetNames() const noexcept = 0;
    };
}
