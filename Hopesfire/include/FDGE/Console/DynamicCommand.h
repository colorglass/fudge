#pragma once

#include <FDGE/Console/CommandRepository.h>

#include "Command.h"

namespace FDGE::Console {
    class DynamicCommand : public Command {
    public:
        template <class... Args>
        requires(std::same_as<Args, std::string_view>&& ...)
        inline DynamicCommand(std::string_view ns, Args... names, std::function<void(const Execution&)>& handler)
                : _registration(CommandRepository::GetSingleton().Register(CommandSpec(ns, names..., handler))),
                  _ns(ns.data()), _names(names.data()...) {
        }

        template <class... Args>
        requires(std::same_as<Args, std::string_view>&& ...)
        inline DynamicCommand(std::string_view ns, Args... names, std::function<void(const Execution&)>&& handler)
                : _registration(CommandRepository::GetSingleton().Register(CommandSpec(ns, names..., handler))),
                  _ns(ns.data()), _names(names.data()...) {
        }

        [[nodiscard]] std::string_view GetNamespace() const noexcept override;

        [[nodiscard]] std::span<const std::string> GetNames() const noexcept override;

    private:
        std::string _ns;
        std::vector<std::string> _names;
        CommandRegistration _registration;
    };
}
