# Fully Dynamic Game Engine
Fully Dynamic Game Engine is a framework for next-generation Skyrim modding, enabling new types of mods with new
features that eliminate the friction of past modding methods. It operates as a layer on top of CommonLibSSE and SKSE.

## Getting Started (C++)
For a complete example, see the
[FuDGE sample plugin project](https://www.gitlab.com/colorglass/fudge-sample-project-cpp). This project is a more
complete demonstration and has more thorough documentation than you will see here and it is strongly recommended to go
through its source and documentation.

---

To use Fully Dynamic Game Engine, first add the custom Color-Glass Vcpkg repository to your project. Create a file in
the project root called `vcpkg-configuration.json` with the following content.

```json
{
    "registries": [
        {
            "kind": "git",
            "repository": "https://gitlab.com/colorglass/vcpkg-colorglass",
            "baseline": "02e6ce0d7dd8880fdbef555a510598cfc28fc986",
            "packages": [
                "articuno",
                "bethesda-skyrim-scripts",
                "commonlibsse",
                "fully-dynamic-game-engine",
                "gluino",
                "script-extender-common",
                "skse",
                "tsl-hat-trie"
            ]
        }
    ]
}
```

To include the necessary dependencies, add them to your `vcpkg.json` file in the root of the project:

```json
{
    "$schema": "https://raw.githubusercontent.com/microsoft/vcpkg/master/scripts/vcpkg.schema.json",
    "name": "skyrim-ng-sample-plugin",
    "version-string": "1.0.0",
    "description": "Sample SKSE plugin based on Fully Dynamic Game Engine's platform.",
    "homepage": "https://www.skyrimng.com",
    "license": "Apache-2.0",
    "dependencies": [
        "fully-dynamic-game-engine"
    ]
}
```

In your `CMakeLists.txt`, find the FuDGE packages and add Hopesfire as a library. Your project must use C++23 as the
language target.

```cmake
find_package(FullyDynamicGameEngine CONFIG REQUIRED)

# ...

add_library(${PROJECT_NAME} SHARED
        ${headers}
        ${sources}
        ${CMAKE_CURRENT_BINARY_DIR}/version.rc
)

target_compile_features(${PROJECT_NAME}
        PUBLIC
        cxx_std_23
)

target_link_libraries(${PROJECT_NAME}
        PRIVATE
        FullyDynamicGameEngine::Hopesfire
)
```

You should now be able to access Fully Dynamic Game Engine functionality. Use the include directive `#include
<FDGE/Hopesfire.h>` to import all FuDGE functionality. It is recommended you add this line to your precompiled headers.
You do not need to include CommonLibSSE headers (and in fact you should not, as there are conditions under which
CommonLibSSE inclusion can fail which FuDGE handles for you, ensuring it is imported correctly; all CommonLibSSE
functionality will be available by including `Hopesfire.h`).

## Getting Started (Python)
