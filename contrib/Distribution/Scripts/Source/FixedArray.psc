scriptName FixedArray extends RandomAccessCollection hidden

FixedArray function Create(Int size) global native

FixedArray function Resize(Int size) native
