scriptName Error extends ScriptObject hidden

Error function Create(String reasonFormat = "", String[] reasonArguments = None, Error cause = None, Bool includeStackTrace = true) global native

Error property Cause
    Error function get()
        return __GetCause()
    endFunction
endProperty
Error function __GetCause() native

String property Reason
    String function get()
        return __GetReason()
    endFunction
endProperty
String function __GetReason() native

StackFrame[] property StackTrace
    StackFrame[] function get()
        return __GetStackTrace()
    endFunction
endProperty
StackFrame[] function __GetStackTrace() native
