scriptName SequenceCollection extends Collection hidden

Bool function Contains(Any value) native

Int function Count(Any value) native

Int function IndexOf(Any value) native
