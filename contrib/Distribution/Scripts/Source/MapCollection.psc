scriptName MapCollection extends Collection hidden

Any function Get(Any key) native

Any function Put(Any key, Any value) native

Bool function TryPut(Any key, Any value) native

Bool function Contains(Any key) native

Any function Delete(Any key) native

KeyValueIterator function GetKeyValueIterator() native
