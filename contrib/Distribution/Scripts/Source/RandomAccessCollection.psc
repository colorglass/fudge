scriptName RandomAccessCollection extends SequenceCollection hidden

Any function Get(Int index) native

Bool function Set(Int index, Any value) native

Any function Erase(Int index) native

Int function ReverseIndexOf(Any value) native

ValueIterator function GetReverseValueIterator() native

ValueIterator function GetValueIteratorFrom(Int index) native

ValueIterator function GetReverseValueIteratorFrom(Int index) native
