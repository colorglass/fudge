scriptName ActiveMagicEffectExt hidden

ActiveMagicEffect[] function GetActiveMagicEffects(Actor target, MagicEffect effect) global native

ActiveMagicEffect function GetActiveMagicEffectObject(ActiveMagicEffect target, String className) global native
