scriptName LocationExtTestSuite extends AutoTestSuite hidden

function TestCreateWrapper()
    Location loc = Game.GetForm(0x13163) as Location
    Assert.IsFormNotNone(loc)
    LocationExt extLoc = LocationExt.Extend(loc)
    Assert.IsFormNotNone(extLoc)
endFunction

function TestCreateNoneWrapper()
    Assert.IsFormNone(LocationExt.Extend(None))
endFunction

function TestGetAllParents()
    LocationExt extLoc = LocationExt.Extend(Game.GetForm(0x13163) as Location)
    Assert.IsFormNotNone(extLoc)
    LocationExt[] parents = extLoc.GetAllParents()

    Location whiterunHold = Game.GetForm(0x16772) as Location
    Assert.IsFormNotNone(whiterunHold)
    Location tamriel = Game.GetForm(0x130FF) as Location
    Assert.IsFormNotNone(tamriel)

    Assert.AreIntsEqual(2, parents.Length)
    Assert.AreFormsEqual(whiterunHold, parents[0])
    Assert.AreFormsEqual(tamriel, parents[1])
endFunction
