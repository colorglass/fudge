scriptName AliasExtTestSuite extends AutoTestSuite hidden

function TestGetAliasByNameNoneQuest()
    Assert.IsAliasNone(AliasExt.GetAliasByName(None, "AstridRef"))
endFunction

function TestGetAliasByNameNonExistent()
    Quest dbQuest = Game.GetForm(0x1EA5C) as Quest
    Assert.IsFormNotNone(dbQuest)
    Assert.IsAliasNone(AliasExt.GetAliasByName(dbQuest, "-bad Name"))
endFunction

function TestGetAliasByNameGood()
    Quest dbQuest = Game.GetForm(0x1EA5C) as Quest
    Assert.IsFormNotNone(dbQuest)
    Assert.IsAliasNotNone(AliasExt.GetAliasByName(dbQuest, "AstridRef"))
endFunction

function TestGetAliasByIDNoneQuest()
    Assert.IsAliasNone(AliasExt.GetAliasByID(None, 0))
endFunction

function TestGetAliasByIDNonExistent()
    Quest dbQuest = Game.GetForm(0x1EA5C) as Quest
    Assert.IsFormNotNone(dbQuest)
    Assert.IsAliasNone(AliasExt.GetAliasByID(dbQuest, 500))
endFunction

function TestGetAliasByIDGood()
    Quest dbQuest = Game.GetForm(0x1EA5C) as Quest
    Assert.IsFormNotNone(dbQuest)
    Alias a = AliasExt.GetAliasByID(dbQuest, 1)
    Assert.IsAliasNotNone(a)
    Assert.AreStringsEqual("AstridRef", a.GetName())
endFunction

function TestGetAliasObjectNonExistent()
    Quest stablesQuest = Game.GetForm(0x68D73) as Quest
    Assert.IsFormNotNone(stablesQuest)
    Alias a = AliasExt.GetAliasByID(stablesQuest, 1)
    Assert.IsAliasNotNone(a)
    Assert.IsAliasNone(AliasExt.GetAliasObject(a, "-bad alias name"))
endFunction

function TestGetAliasObjectNoneAlias()
    Assert.IsAliasNone(AliasExt.GetAliasObject(None, "CWDisableDuringSiegeALIASScript"))
endFunction

function TestGetAliasObjectGood()
    Quest stablesQuest = Game.GetForm(0x68D73) as Quest
    Assert.IsFormNotNone(stablesQuest)
    Alias a = AliasExt.GetAliasByID(stablesQuest, 9)
    Assert.IsAliasNotNone(a)
    Alias siegeRef = AliasExt.GetAliasObject(a, "CWDisableDuringSiegeALIASScript")
    Assert.IsAliasNotNone(siegeRef)
    Assert.IsAliasNotNone(siegeRef as CWDisableDuringSiegeALIASScript)
endFunction
