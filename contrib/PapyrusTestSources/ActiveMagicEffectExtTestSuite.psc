scriptName ActiveMagicEffectExtTestSuite extends AutoTestSuite hidden

function TestGetActiveEffectsNoneActor()
    MagicEffect effect = Game.GetForm(0x17120) as MagicEffect
    Assert.IsFormNotNone(effect)

    ActiveMagicEffect[] effects = ActiveMagicEffectExt.GetActiveMagicEffects(None, effect)
    Assert.AreIntsEqual(0, effects.Length, "Expected to get array of 0 active effects.")
endFunction

function TestGetActiveEffectsNoneEffect()
    Actor player = Game.GetPlayer()
    Assert.IsFormNotNone(player)

    ActiveMagicEffect[] effects = ActiveMagicEffectExt.GetActiveMagicEffects(player, None)
    Assert.AreIntsEqual(0, effects.Length, "Expected to get array of 0 active effects.")
endFunction

function TestGetActiveEffectsNonExistent()
    Actor player = Game.GetPlayer()
    Assert.IsFormNotNone(player)

    MagicEffect effect = Game.GetForm(0x1CEA0) as MagicEffect
    Assert.IsFormNotNone(effect)

    ActiveMagicEffect[] effects = ActiveMagicEffectExt.GetActiveMagicEffects(player, effect)
    Assert.AreIntsEqual(0, effects.Length, "Expected to get array of 0 active effects.")
endFunction

function TestGetActiveEffects()
    Actor player = Game.GetPlayer()
    Assert.IsFormNotNone(player)

    MagicEffect effect = Game.GetForm(0x17120) as MagicEffect
    Assert.IsFormNotNone(effect)

    ActiveMagicEffect[] effects = ActiveMagicEffectExt.GetActiveMagicEffects(player, effect)
    Assert.IsTrue(effects.Length >= 1, "Expected to get array that includes active effect.")

    Int i = 0
    while i < effects.Length
        Assert.IsActiveMagicEffectNotNone(effects[i])
        i += 1
    endWhile
endFunction
